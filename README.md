# chartas

Backend part of platform for sharing and managing historical events in both temporal and spatial dimensions.

To start server type:

./gradlew run


Remember to set appropriate values in src/main/resources/application.conf

optimization - select optimization search algorithm, from "tree", "bisection", "clustering", other will cause default sequential optimization


persistence - select driver for external database, from "neo4j", other will cause default in memory storage


persistence_address - uri and protocol to communicate driver with external database 


persistence_user = username for account that driver will use during authentication


persistence_password = password for account that driver will use during authentication
