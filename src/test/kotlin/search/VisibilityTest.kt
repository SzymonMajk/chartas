package search

import domain.CoronationStub
import domain.Event
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator

/**
 * TODO refactor to not be sooo tightly coupled with CoronationStub
 */
internal class VisibilityTest {

    private val generator = CountingGenerator()
    private val coronationTour = CoronationStub(generator).coronationTour()
    private val fuzzyInterval = coronationTour.interval as FuzzyInterval
    private val coronationCelebration = CoronationStub(generator).coronationCelebration()
    private val exactInterval = coronationCelebration.interval as ExactInterval

    private fun resetOptimizer(optimizer : SearchOptimizer, events : Collection<Event>) {
        events.forEach { optimizer.add(it) }
    }

    private fun deepEqualToIgnoreOrder(expected: Collection<Event>, received: Collection<Event>): Boolean {
        if (expected !== received) {
            if (expected.size != received.size) return false
            val areNotEqual = expected.asSequence()
                .map { it in received }
                .contains(false)
            if (areNotEqual) return false
        }

        return true
    }

    @Test
    fun intervalLongerThanBrowserShouldBeVisible() {
        val dayInside = fuzzyInterval.date.plusDays((fuzzyInterval.duration / 2).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayInside, dayInside)))
    }

    @Test
    fun intervalWithinBrowserShouldBeVisible() {
        val dayBefore = fuzzyInterval.date.minusDays(1)
        val dayAfter = fuzzyInterval.date.plusDays((fuzzyInterval.duration + 1).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayBefore, dayAfter)))
    }

    @Test
    fun intervalLeftIntersectingBrowserShouldBeVisible() {
        val dayBefore = fuzzyInterval.date.minusDays(1)
        val dayLeftEdge = fuzzyInterval.date
        val dayInside = fuzzyInterval.date.plusDays((fuzzyInterval.duration / 2).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayBefore, dayInside)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayBefore, dayLeftEdge)))
    }

    @Test
    fun intervalRightIntersectingBrowserShouldBeVisible() {
        val dayInside = fuzzyInterval.date.plusDays((fuzzyInterval.duration / 2).toLong())
        val dayRightEdge = fuzzyInterval.date.plusDays(fuzzyInterval.duration.toLong())
        val dayAfter = fuzzyInterval.date.plusDays((fuzzyInterval.duration + 1).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayInside, dayAfter)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayRightEdge, dayAfter)))
    }

    @Test
    fun browserWithinIntervalShouldBeVisible() {
        val dayInside = fuzzyInterval.date.plusDays((fuzzyInterval.duration / 2).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayInside, dayInside)))
    }

    @Test
    fun intervalOutOfBrowserShouldNotBeVisibleWhenStrictThreshold() {
        val twoDaysBefore = fuzzyInterval.date.minusDays(2)
        val dayBefore = fuzzyInterval.date.minusDays(1)
        val dayAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 1).toLong())
        val twoDaysAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 2).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(twoDaysBefore, dayBefore, 1.0)))

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayAfter, twoDaysAfter, 1.0)))
    }

    @Test
    fun intervalOutOfBrowserShouldBeVisibleWhenNonStrictThreshold() {
        val twoDaysBefore = fuzzyInterval.date.minusDays(2)
        val dayBefore = fuzzyInterval.date.minusDays(1)
        val dayAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 1).toLong())
        val twoDaysAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 2).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(twoDaysBefore, dayBefore, 0.0)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayAfter, twoDaysAfter, 0.0)))
    }

    @Test
    fun thresholdShouldChangeVisibility() {
        val twoDaysBefore  = fuzzyInterval.date.minusDays(2)
        val oneDayBefore = fuzzyInterval.date.minusDays(1)
        val fiveDaysAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 5).toLong())
        val sixDaysAfter= fuzzyInterval.date.plusDays((fuzzyInterval.duration + 6).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(twoDaysBefore, oneDayBefore, 0.0)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(fiveDaysAfter, sixDaysAfter, 0.0)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(oneDayBefore, fiveDaysAfter, 1.0)))

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(twoDaysBefore, oneDayBefore, 1.0)))

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(fiveDaysAfter, sixDaysAfter, 1.0)))
    }

    @Test
    fun intervalWithZeroDurationLength() {
        val twoDaysBefore = exactInterval.date.minusDays(2)
        val dayBefore = exactInterval.date.minusDays(1)
        val eventDay = exactInterval.date
        val dayAfter = exactInterval.date.plusDays((exactInterval.duration + 1).toLong())
        val twoDaysAfter = exactInterval.date.plusDays((exactInterval.duration + 2).toLong())
        val events = arrayListOf(coronationCelebration)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(dayBefore, dayAfter)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(twoDaysBefore, eventDay)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(eventDay, eventDay)))

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(eventDay, twoDaysAfter)))

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(twoDaysBefore, dayBefore)))

        assertFalse(deepEqualToIgnoreOrder(
            arrayListOf(coronationCelebration),
            sequentialOptimizer.getVisible(dayAfter, twoDaysAfter)))
    }

    @Test
    fun invertedBrowserBorders() {
        val dayBefore = fuzzyInterval.date.minusDays(1)
        val dayAfter = fuzzyInterval.date.plusDays((fuzzyInterval.duration + 1).toLong())
        val events = arrayListOf(coronationTour)
        val sequentialOptimizer = SequentialOptimizer()
        resetOptimizer(sequentialOptimizer, events)

        assertTrue(deepEqualToIgnoreOrder(
            arrayListOf(coronationTour),
            sequentialOptimizer.getVisible(dayAfter, dayBefore)))
    }
}