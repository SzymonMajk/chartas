package search

import domain.Event
import domain.temporal.ExactInterval
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdBase
import repository.identification.IdGenerator
import search.bisection.BisectionOptimizer
import search.clustering.IntervalClusteringOptimizer
import search.tree.IntervalTreeOptimizer
import java.time.LocalDate

internal class ValidationTest {

    private val startRange = LocalDate.of(875,1,1)
    private val endRange = startRange.plusDays(81)
    private lateinit var ids : IdGenerator

    private fun resetOptimizer(optimizer : SearchOptimizer, events : Collection<Event>) {
        events.forEach { optimizer.add(it) }
    }

    @BeforeEach
    fun initGenerator() {
        ids = CountingGenerator()
    }

    private fun prepareEvent(date : LocalDate, duration : Int) : Event {
        return Event(
                ids.nextId(IdBase.EVENT),
            "name",
            "desc",
                ExactInterval(
                    ids.nextId(IdBase.INTERVAL),
                    date,
                    duration
            )
        )
    }

    private fun shouldFindOneEvent(optimizer : SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(5), 20),
            prepareEvent(startRange.plusDays(10), 10),
            prepareEvent(startRange.plusDays(1), 5),
            prepareEvent(startRange.plusDays(12), 12),
            prepareEvent(startRange.plusDays(30), 5)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(32),
                startRange.plusDays(36))
            .toSet()

        assertTrue(events.subList(4,5).toSet() == resultSet)
    }

    private fun shouldFindReversedOneEvent(optimizer : SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(30), 5),
            prepareEvent(startRange.plusDays(12), 12),
            prepareEvent(startRange.plusDays(10), 10),
            prepareEvent(startRange.plusDays(5), 20),
            prepareEvent(startRange.plusDays(1), 5)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(32),
                startRange.plusDays(36))
            .toSet()

        assertTrue(events.subList(0,1).toSet() == resultSet)
    }

    private fun shouldFindAddedFromBorders(optimizer : SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(30), 5),
            prepareEvent(startRange.plusDays(1), 5),
            prepareEvent(startRange.plusDays(12), 12),
            prepareEvent(startRange.plusDays(10), 10),
            prepareEvent(startRange.plusDays(5), 20)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(32),
                startRange.plusDays(36))
            .toSet()

        assertTrue(events.subList(0,1).toSet() == resultSet)
    }

    private fun shouldFindNoneEvent(optimizer : SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(5), 20),
            prepareEvent(startRange.plusDays(10), 10),
            prepareEvent(startRange.plusDays(29), 5),
            prepareEvent(startRange.plusDays(32), 12),
            prepareEvent(startRange.plusDays(1), 10)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(26),
                startRange.plusDays(28))
            .toSet()

        assertTrue(resultSet.isEmpty())
    }

    private fun shouldFindWithVeryLongEvent(optimizer: SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(3), 2),
            prepareEvent(startRange.plusDays(8), 40),
            prepareEvent(startRange.plusDays(7), 5),
            prepareEvent(startRange.plusDays(12), 12),
            prepareEvent(startRange.plusDays(25), 13),
            prepareEvent(startRange.plusDays(34), 15)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(9),
                startRange.plusDays(11))
            .toSet()

        assertTrue(events.subList(1,3).toSet() == resultSet)
    }

    private fun shouldDealWithThreeGroups(optimizer : SearchOptimizer) {
        val events = listOf(
            prepareEvent(startRange.plusDays(3), 2),
            prepareEvent(startRange.plusDays(5), 12),
            prepareEvent(startRange.plusDays(9), 4),
            prepareEvent(startRange.plusDays(24), 12),
            prepareEvent(startRange.plusDays(28), 3),
            prepareEvent(startRange.plusDays(31), 20),
            prepareEvent(startRange.plusDays(62), 3),
            prepareEvent(startRange.plusDays(66), 7),
            prepareEvent(startRange.plusDays(67), 13)
        )
        resetOptimizer(optimizer, events)

        val resultSet = optimizer
            .getVisible(
                startRange.plusDays(24),
                startRange.plusDays(28))
            .toSet()

        assertTrue(events.subList(3, 5).toSet() == resultSet)
    }

    @Test
    fun sequentialOptimizerShouldBeValid() {
        shouldFindOneEvent(SequentialOptimizer())
        shouldFindReversedOneEvent(SequentialOptimizer())
        shouldFindNoneEvent(SequentialOptimizer())
        shouldFindWithVeryLongEvent(SequentialOptimizer())
        shouldDealWithThreeGroups(SequentialOptimizer())
    }

    @Test
    fun bisectionOptimizerShouldBeValid() {
        shouldFindOneEvent(BisectionOptimizer())
        shouldFindReversedOneEvent(BisectionOptimizer())
        shouldFindAddedFromBorders(BisectionOptimizer())
        shouldFindNoneEvent(BisectionOptimizer())
        shouldFindWithVeryLongEvent(BisectionOptimizer())
        shouldDealWithThreeGroups(BisectionOptimizer())
    }

    @Test
    fun treeOptimizerShouldBeValid() {
        shouldFindOneEvent(IntervalTreeOptimizer(4, 7, startRange, endRange))
        shouldFindReversedOneEvent(IntervalTreeOptimizer(4, 7, startRange, endRange))
        shouldFindNoneEvent(IntervalTreeOptimizer(4, 7, startRange, endRange))
        shouldFindWithVeryLongEvent(IntervalTreeOptimizer(4, 7, startRange, endRange))
        shouldDealWithThreeGroups(IntervalTreeOptimizer(4, 7, startRange, endRange))

        shouldFindOneEvent(IntervalTreeOptimizer(2, 10, startRange, endRange))
        shouldFindReversedOneEvent(IntervalTreeOptimizer(2, 10, startRange, endRange))
        shouldFindNoneEvent(IntervalTreeOptimizer(2, 10, startRange, endRange))
        shouldFindWithVeryLongEvent(IntervalTreeOptimizer(2, 10, startRange, endRange))
        shouldDealWithThreeGroups(IntervalTreeOptimizer(2, 10, startRange, endRange))
    }

    @Test
    fun clusteringOptimizerShouldBeValid() {
        shouldFindOneEvent(IntervalClusteringOptimizer(1))
        shouldFindReversedOneEvent(IntervalClusteringOptimizer(1))
        shouldFindNoneEvent(IntervalClusteringOptimizer(1))
        shouldFindWithVeryLongEvent(IntervalClusteringOptimizer(1))
        shouldDealWithThreeGroups(IntervalClusteringOptimizer(1))
    }
}