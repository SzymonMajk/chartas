package search

import domain.Event
import domain.temporal.ExactInterval
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdBase
import repository.identification.IdGenerator
import search.bisection.BisectionOptimizer
import search.clustering.IntervalClusteringOptimizer
import search.tree.IntervalTreeOptimizer
import java.time.LocalDate
import kotlin.random.Random
import kotlin.system.measureNanoTime

internal class ComparisonTest {

    private val startRange = LocalDate.of(875,1,1)
    private val periodMax : Long = 365 * 259
    private val endRange = startRange.plusDays(periodMax)
    private val durationMax = 20
    private lateinit var generator : Random
    private lateinit var ids : IdGenerator

    private fun resetOptimizer(optimizer : SearchOptimizer, events : Collection<Event>) {
        events.forEach { optimizer.add(it) }
    }

    private fun generateUntil(generator : Random, count : Int = 100000) : Collection<Event> {
        val generated =  emptyList<Event>().toMutableList()

        for (i in 1..count) {
            generated.add(
                Event(
                ids.nextId(IdBase.EVENT),
                    "name",
                    "desc",
                    ExactInterval(
                        ids.nextId(IdBase.INTERVAL),
                        startRange.plusDays(generator.nextLong(0, periodMax)),
                        generator.nextInt(durationMax)
                    )
                )
            )
        }

        return generated
    }

    private fun measureResetInsertTime(optimizer : SearchOptimizer, events : Collection<Event>) {
        val time = measureNanoTime {
            resetOptimizer(optimizer, events)
        }
        println("Reset: $time nanos")

    }

    private fun measureSingleInsertTime(optimizer : SearchOptimizer, event : Event) {
        val time = measureNanoTime {
            optimizer.add(event)
        }
        println("Insert: $time nanos")
    }

    private fun measureFindTime(optimizer: SearchOptimizer, start: LocalDate, end: LocalDate) {
        var count = 0

        val time = measureNanoTime {
            count = optimizer.getVisible(start, end).size
        }

        println("Search $time nanos")
        println("$count events found")
    }

    private fun proceedMeasurements(name : String, optimizer: SearchOptimizer) {
        println(name)
        measureResetInsertTime(optimizer, generateUntil(generator))
        measureSingleInsertTime(optimizer, generateUntil(generator, 1).toList()[0])
        measureFindTime(optimizer, startRange.plusDays(50000), startRange.plusDays(50100))

    }

    @BeforeEach
    fun initGenerator() {
        ids = CountingGenerator()
        generator = Random(117)
    }

    @Test
    fun sequentialOptimizer() {
        proceedMeasurements("Sequential", SequentialOptimizer())
    }

    @Test
    fun bisectionOptimizer() {
        proceedMeasurements("Bisection", BisectionOptimizer())
    }

    @Test
    fun treeOptimizer() {
        proceedMeasurements("Tree", IntervalTreeOptimizer(4, 7, startRange, endRange))
    }

    @Test
    fun clusteringOptimizer() {
        proceedMeasurements("Clustering", IntervalClusteringOptimizer(2))
    }
}