package export

import domain.CoronationStub
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdGenerator

/**
 * TODO check what when 2 events have same interval, there should be addtional check
 */
internal class GraphJsonExportTest {

    private lateinit var generator: IdGenerator
    private lateinit var coronationStub: CoronationStub
    private val initialState = "{}"

    @BeforeEach
    fun initGenerator() {
        generator = CountingGenerator()
        coronationStub = CoronationStub(generator)
    }

    @Test
    fun eventDetailsShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(0)

        Assertions.assertEquals(
            "Event",
            serializedEvent.getString("label")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvent.getString("id")
        )
        Assertions.assertEquals(
            coronation.name,
            serializedEvent.getString("name")
        )
        Assertions.assertEquals(
            coronation.description,
            serializedEvent.getString("description")
        )
    }

    @Test
    fun exactIntervalShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationCelebration()
        val interval = coronation.interval as ExactInterval

        val serializedInterval = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(1)

        Assertions.assertEquals(
            "Interval",
            serializedInterval.getString("label")
        )
        Assertions.assertEquals(
            interval.id.toString(),
            serializedInterval.getString("id")
        )
        Assertions.assertEquals(
            interval.date.toString(),
            serializedInterval.getString("start")
        )
        Assertions.assertEquals(
            interval.date.plusDays(interval.duration.toLong()).toString(),
            serializedInterval.getString("end")
        )
    }

    @Test
    fun fuzzyIntervalShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationTour()
        val interval = coronation.interval as FuzzyInterval

        val serializedInterval = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(1)

        Assertions.assertEquals(
            "Interval",
            serializedInterval.getString("label")
        )
        Assertions.assertEquals(
            interval.id.toString(),
            serializedInterval.getString("id")
        )
        Assertions.assertEquals(
            interval.date.toString(),
            serializedInterval.getString("start")
        )
        Assertions.assertEquals(
            interval.date.plusDays(interval.duration.toLong()).toString(),
            serializedInterval.getString("end")
        )
        Assertions.assertEquals(
            interval.commenceUncertainty.toString(),
            serializedInterval.getString("commenceUncertainty")
        )
        Assertions.assertEquals(
            interval.terminationUncertainty.toString(),
            serializedInterval.getString("terminationUncertainty")
        )
    }

    @Test
    fun referencesShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedReference = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(3)

        Assertions.assertEquals(
            "Reference",
            serializedReference.getString("label")
        )
        Assertions.assertEquals(
            coronation.references.toList()[0].id.toString(),
            serializedReference.getString("id")
        )
        Assertions.assertEquals(
            coronation.references.toList()[0].title,
            serializedReference.getString("title")
        )
        Assertions.assertEquals(
            coronation.references.toList()[0].author,
            serializedReference.getString("author")
        )
        Assertions.assertEquals(
            coronation.references.toList()[0].source,
            serializedReference.getString("source")
        )
    }

    @Test
    fun pointCoordinatesShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedCoordinates = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(2)

        Assertions.assertEquals(
            "Coordinates",
            serializedCoordinates.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedCoordinates.getString("id")
        )
        Assertions.assertTrue(
            Utils.assertArrayJSONArray(
                (coronation.coordinates as PointCoordinates).lonLat,
                serializedCoordinates.getJSONArray("coordinates")
            )
        )
    }

    @Test
    fun lineCoordinatesShouldSerializeToGraphJSON() {
        val coronation = coronationStub.coronationTour()

        val serializedCoordinates = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(2)

        Assertions.assertEquals(
            "Coordinates",
            serializedCoordinates.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedCoordinates.getString("id")
        )
        Assertions.assertTrue(
            Utils.assertArrayJSONArray(
                (coronation.coordinates as LineCoordinates).points,
                serializedCoordinates.getJSONArray("coordinates")
            )
        )
    }

    @Test
    fun areaCoordinatesShouldSerializeToGraphJSON() {
        val coronation = coronationStub.kingdomCreated()

        val serializedCoordinates = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(2)

        Assertions.assertEquals(
            "Coordinates",
            serializedCoordinates.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedCoordinates.getString("id")
        )
        Assertions.assertTrue(
            Utils.assertArrayJSONArray(
                (coronation.coordinates as PolygonCoordinates).polygons,
                serializedCoordinates.getJSONArray("coordinates")
            )
        )
    }

    @Test
    fun eventRelationshipShouldExistInGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("nodes").getJSONObject(0)

        Assertions.assertEquals(
            "Event",
            serializedEvent.getString("label")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvent.getString("id")
        )
        Assertions.assertEquals(
            coronation.name,
            serializedEvent.getString("name")
        )
    }

    @Test
    fun eventShouldConnectOtherEventInGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(3)

        Assertions.assertEquals(
            coronation.outgoingRelationships.toList()[0].label,
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.outgoingRelationships.toList()[0].from.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.outgoingRelationships.toList()[0].to.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun eventShouldConnectReferenceInGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(2)

        Assertions.assertEquals(
            "REFERENCED",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.references.toList()[0].id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun eventShouldConnectPointCoordinatesInGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(1)

        Assertions.assertEquals(
            "WHERE",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun eventShouldConnectLineCoordinatesInGraphJSON() {
        val coronation = coronationStub.coronationTour()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(1)

        Assertions.assertEquals(
            "WHERE",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun eventShouldConnectPolygonCoordinatesInGraphJSON() {
        val coronation = coronationStub.kingdomCreated()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(1)

        Assertions.assertEquals(
            "WHERE",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun eventShouldConnectExactIntervalInGraphJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(0)

        Assertions.assertEquals(
            "WHEN",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.interval?.id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }


    @Test
    fun eventShouldConnectFuzzyIntervalInGraphJSON() {
        val coronation = coronationStub.coronationTour()

        val serializedEdge = JSONObject(coronation.accept(GraphJsonExporter(), initialState))
            .getJSONArray("edges").getJSONObject(0)

        Assertions.assertEquals(
            "WHEN",
            serializedEdge.getString("label")
        )
        Assertions.assertEquals(
            coronation.interval?.id.toString(),
            serializedEdge.getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEdge.getString("from")
        )
    }

    @Test
    fun twoEventsShouldConnectWithSameIntervalNode() {
        val coronation = coronationStub.coronationCelebration()
        val exporter = GraphJsonExporter()
        val interval = coronation.interval as ExactInterval

        val afterFirstEvent = JSONObject(coronation.accept(exporter, initialState)).toString()
        val serializedEvents = JSONObject(coronation.accept(exporter, afterFirstEvent))

        Assertions.assertEquals(5, serializedEvents.getJSONArray("nodes").length())
        Assertions.assertEquals(8, serializedEvents.getJSONArray("edges").length())

        val serializedInterval = serializedEvents.getJSONArray("nodes").getJSONObject(1)

        Assertions.assertEquals(
            "Interval",
            serializedInterval.getString("label")
        )
        Assertions.assertEquals(
            interval.id.toString(),
            serializedInterval.getString("id")
        )
        Assertions.assertEquals(
            interval.date.toString(),
            serializedInterval.getString("start")
        )
        Assertions.assertEquals(
            interval.date.plusDays(interval.duration.toLong()).toString(),
            serializedInterval.getString("end")
        )

        Assertions.assertEquals(
            "WHEN",
            serializedEvents.getJSONArray("edges").getJSONObject(0).getString("label")
        )
        Assertions.assertEquals(
            coronation.interval?.id.toString(),
            serializedEvents.getJSONArray("edges").getJSONObject(0).getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvents.getJSONArray("edges").getJSONObject(0).getString("from")
        )
        Assertions.assertEquals(
            "WHEN",
            serializedEvents.getJSONArray("edges").getJSONObject(4).getString("label")
        )
        Assertions.assertEquals(
            coronation.interval?.id.toString(),
            serializedEvents.getJSONArray("edges").getJSONObject(4).getString("to")
        )
        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvents.getJSONArray("edges").getJSONObject(4).getString("from")
        )
    }
}