package export

import domain.CoronationStub
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdGenerator


internal class TimelineJsonExportTest {
    private lateinit var generator: IdGenerator
    private lateinit var coronationStub: CoronationStub
    private val initialState = "{}"

    @BeforeEach
    fun initGenerator() {
        generator = CountingGenerator()
        coronationStub = CoronationStub(generator)
    }

    @Test
    fun eventDetailsShouldSerializeToTimelineJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(TimelineJsonExporter(), initialState))
            .getJSONArray("events").getJSONObject(0)

        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvent.getString("id")
        )
        Assertions.assertEquals(
            coronation.name,
            serializedEvent.getString("content")
        )
        Assertions.assertEquals(
            coronation.description,
            serializedEvent.getString("description")
        )
    }

    @Test
    fun intervalShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationCelebration()
        val interval = coronation.interval as ExactInterval

        val serializedEvent = JSONObject(coronation.accept(TimelineJsonExporter(), initialState))
            .getJSONArray("events").getJSONObject(0)

        Assertions.assertEquals(
            interval.date.toString(),
            serializedEvent.getString("start")
        )
        Assertions.assertEquals(
            interval.date.plusDays(interval.duration.toLong()).toString(),
            serializedEvent.getString("end")
        )
    }

    @Test
    fun fuzzyIntervalShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationTour()
        val interval = coronation.interval as FuzzyInterval

        val serializedEvent = JSONObject(coronation.accept(TimelineJsonExporter(), initialState))
            .getJSONArray("events").getJSONObject(0)

        Assertions.assertEquals(
            interval.date.minusDays(interval.commenceUncertainty.toLong()).toString(),
            serializedEvent.getString("start")
        )
        Assertions.assertEquals(
            interval.date.plusDays((interval.duration + interval.terminationUncertainty).toLong()).toString(),
            serializedEvent.getString("end")
        )
    }

    @Test
    fun referencesShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(TimelineJsonExporter(), initialState))
            .getJSONArray("events").getJSONObject(0)

        Assertions.assertEquals(
            coronation.references.map { "\"" + it.title + "\"" }.toString(),
            serializedEvent.getString("references")
        )
    }
}