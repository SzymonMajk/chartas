package export

import domain.CoronationStub
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONObject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import repository.identification.CountingGenerator
import repository.identification.IdGenerator

internal class GeoJsonExportTest {

    private lateinit var generator : IdGenerator
    private lateinit var coronationStub : CoronationStub
    private val initialState = "{}"

    @BeforeEach
    fun initGenerator() {
        generator = CountingGenerator()
        coronationStub = CoronationStub(generator)
    }

    @Test
    fun eventDetailsShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            "Feature",
            serializedEvent.getString("type"))
        assertEquals(
            coronation.id.toString(),
            serializedEvent.getJSONObject("properties").getString("id"))
        assertEquals(
            coronation.name,
            serializedEvent.getJSONObject("properties").getString("name"))
        assertEquals(
            coronation.description,
            serializedEvent.getJSONObject("properties").getString("description"))
    }

    @Test
    fun exactIntervalShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationCelebration()
        val interval = coronation.interval as ExactInterval

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            interval.date.toString(),
            serializedEvent.getJSONObject("properties").getString("date"))
        assertEquals(
            interval.duration.toString(),
            serializedEvent.getJSONObject("properties").getString("duration"))
    }

    @Test
    fun fuzzyIntervalShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationTour()
        val interval = coronation.interval as FuzzyInterval

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            interval.date.toString(),
            serializedEvent.getJSONObject("properties").getString("date"))
        assertEquals(
            interval.duration.toString(),
            serializedEvent.getJSONObject("properties").getString("duration"))
        assertEquals(
            interval.commenceUncertainty.toString(),
            serializedEvent.getJSONObject("properties").getString("commenceUncertainty"))
        assertEquals(
            interval.terminationUncertainty.toString(),
            serializedEvent.getJSONObject("properties").getString("terminationUncertainty"))
    }

    @Test
    fun referencesShouldSerializeToGeoJSON(){
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            coronation.references.map { "\"" + it.title + "\"" }.toString(),
            serializedEvent.getJSONObject("properties").getString("references"))
    }

    @Test
    fun pointCoordinatesShouldSerializeToGeoJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            "Point",
            serializedEvent.getJSONObject("geometry").getString("type"))
        assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEvent.getJSONObject("geometry").getString("id"))
        assertTrue(Utils.assertArrayJSONArray(
            (coronation.coordinates as PointCoordinates).lonLat,
            serializedEvent.getJSONObject("geometry").getJSONArray("coordinates")
        ))
    }

    @Test
    fun lineCoordinatesShouldSerializeToGeoJSON() {
        val coronation = coronationStub.coronationTour()

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            "LineString",
            serializedEvent.getJSONObject("geometry").getString("type")
        )
        assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEvent.getJSONObject("geometry").getString("id")
        )
        assertTrue(Utils.assertArrayJSONArray(
            (coronation.coordinates as LineCoordinates).points,
            serializedEvent.getJSONObject("geometry").getJSONArray("coordinates")
        ))
    }

    @Test
    fun areaCoordinatesShouldSerializeToGeoJSON() {
        val coronation = coronationStub.kingdomCreated()

        val serializedEvent = JSONObject(coronation.accept(GeoJsonExporter(), initialState))
            .getJSONArray("features").getJSONObject(0)

        assertEquals(
            "Polygon",
            serializedEvent.getJSONObject("geometry").getString("type")
        )
        assertEquals(
            coronation.coordinates?.id.toString(),
            serializedEvent.getJSONObject("geometry").getString("id")
        )
        assertTrue(Utils.assertArrayJSONArray(
            (coronation.coordinates as PolygonCoordinates).polygons,
            serializedEvent.getJSONObject("geometry").getJSONArray("coordinates")
        ))
    }
}