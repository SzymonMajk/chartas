package export

import org.json.JSONArray

object Utils {

    fun assertArrayJSONArray(expected: Array<Double>, received: JSONArray): Boolean {
        if (expected !== received) {
            if (expected.size != received.length()) return false

            for (i in 0 until received.length()) {
                if (expected[i] != received.get(i) as Double) return false
            }
        }

        return true
    }

    fun assertArrayJSONArray(expected: Array<Array<Double>>, received: JSONArray): Boolean {
        if (expected !== received) {
            if (expected.size != received.length()) return false
            for (i in 0 until received.length()) {

                if (expected[i].size != received.getJSONArray(i).length()) return false
                for (j in 0 until received.getJSONArray(i).length()) {
                    if (expected[i][j] != received.getJSONArray(i).get(j) as Double) return false
                }
            }
        }

        return true
    }

    fun assertArrayJSONArray(expected: Array<Array<Array<Double>>>, received: JSONArray): Boolean {
        if (expected !== received) {
            if (expected.size != received.length()) return false
            for (i in 0 until received.length()) {

                if (expected[i].size != received.getJSONArray(i).length()) return false
                for (j in 0 until received.getJSONArray(i).length()) {

                    if (expected[i][j].size != received.getJSONArray(i).getJSONArray(j).length()) return false
                    for (k in 0 until received.getJSONArray(i).getJSONArray(j).length()) {
                        if (expected[i][j][k] != received.getJSONArray(i).getJSONArray(j).get(k) as Double) return false
                    }
                }
            }
        }

        return true
    }
}