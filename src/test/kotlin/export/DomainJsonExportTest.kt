package export

import domain.CoronationStub
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdGenerator

internal class DomainJsonExportTest {
    private lateinit var generator: IdGenerator
    private lateinit var coronationStub: CoronationStub
    private val initialState = "{}"

    @BeforeEach
    fun initGenerator() {
        generator = CountingGenerator()
        coronationStub = CoronationStub(generator)
    }

    @Test
    fun eventWithDetailsAndSingleIdsShouldSerializeToJSON() {
        val coronation = coronationStub.coronationCelebration()

        val serializedEvent = JSONObject(coronation.accept(DomainJsonExporter(), initialState))
            .getJSONArray("events").getJSONObject(0)

        Assertions.assertEquals(
            coronation.id.toString(),
            serializedEvent.getString("id")
        )
        Assertions.assertEquals(
            coronation.name,
            serializedEvent.getString("name")
        )
        Assertions.assertEquals(
            coronation.description,
            serializedEvent.getString("description")
        )
        Assertions.assertEquals(
            coronation.interval!!.id.toString(),
            serializedEvent.getString("intervalId")
        )
        Assertions.assertEquals(
            coronation.coordinates!!.id.toString(),
            serializedEvent.getString("coordinatesId")
        )
        Assertions.assertEquals(1, serializedEvent.getJSONArray("references").length())
        Assertions.assertEquals(
            coronation.references.toList()[0].id.toString(),
            serializedEvent.getJSONArray("references")[0]
        )

        Assertions.assertEquals(1, serializedEvent.getJSONArray("relationships").length())
        Assertions.assertEquals(
            coronation.outgoingRelationships.toList()[0].to.toString(),
            serializedEvent.getJSONArray("relationships")[0]
        )
    }
}