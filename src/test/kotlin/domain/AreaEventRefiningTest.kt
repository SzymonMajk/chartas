package domain

import domain.spatial.PolygonCoordinates
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import repository.identification.CountingGenerator
import repository.identification.IdBase


internal class AreaEventRefiningTest {

    private val generator = CountingGenerator()

    private fun assertArrayOfArrays(expected: Array<Array<Array<Double>>>, actual: Array<Array<Array<Double>>>) {
        assertEquals(expected.size, actual.size)
        for(i in expected.indices) {
            assertEquals(expected[i].size, actual[i].size)
            for (j in expected[i].indices) {
                assertArrayEquals(expected[i][j], actual[i][j])
            }
        }
    }

    @Test
    fun coordinatesArrayStartsAndEndSameValues() {
        val correctPolygons = PolygonCoordinates(
            generator.nextId(IdBase.COORDINATES),
            arrayOf(arrayOf(
            arrayOf(50.5, 50.6),
            arrayOf(51.5, 51.6),
            arrayOf(52.5, 52.6),
            arrayOf(50.5, 50.6))))
        val refinedPolygons = PolygonCoordinates(
            generator.nextId(IdBase.COORDINATES),
            arrayOf(arrayOf(
            arrayOf(50.5, 50.6),
            arrayOf(51.5, 51.6),
            arrayOf(52.5, 52.6))))

        assertArrayOfArrays(correctPolygons.polygons, refinedPolygons.polygons)
    }
}