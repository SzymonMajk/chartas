package domain

import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import domain.temporal.Interval
import repository.identification.IdBase
import repository.identification.IdGenerator
import java.time.LocalDate
import java.time.Month

class CoronationStub(generator : IdGenerator) {

    private val name = "Coronation"
    private val description = "Stub of fiction single, line and area event with common bowels."
    private val exactInterval : Interval = ExactInterval(
        generator.nextId(IdBase.INTERVAL),
        LocalDate.of(1025, Month.APRIL, 18),
        0
    )

    private val fuzzyInterval : Interval = FuzzyInterval(
        generator.nextId(IdBase.INTERVAL),
        LocalDate.of(1025, Month.APRIL, 19),
        2,
        1,
        5
    )

    private var relationships = emptyList<Relationship>().toMutableList()

    private val coronationPalace = PointCoordinates(
        generator.nextId(IdBase.COORDINATES),
        arrayOf(49.9, 47.5)
    )

    private val coronationTour = LineCoordinates(
        generator.nextId(IdBase.COORDINATES),
        arrayOf(arrayOf(49.9, 47.5), arrayOf(49.9, 46.5), arrayOf(48.8, 47.5))
    )

    private val newKingdom = PolygonCoordinates(
        generator.nextId(IdBase.COORDINATES),
        arrayOf(arrayOf(arrayOf(50.2,48.2), arrayOf(50.2, 51.2), arrayOf(47.3,51.2), arrayOf(47.3, 48.2)))
    )

    private val oldBook = Reference(
        generator.nextId(IdBase.REFERENCE),
        "Old book",
        "Wise man",
        "Something..."
    )

    private var coronationCelebrationEvent: Event
    private var coronationTourEvent: Event
    private var coronationKingdomEvent: Event

    init {
        coronationCelebrationEvent = Event(
            generator.nextId(IdBase.EVENT),
            name,
            description,
            exactInterval,
            coronationPalace,
            relationships,
            arrayListOf(oldBook)
        )

        coronationTourEvent = Event(
            generator.nextId(IdBase.EVENT),
            name,
            description,
            fuzzyInterval,
            coronationTour,
            emptyList(),
            arrayListOf(oldBook)
        )

        coronationKingdomEvent = Event(
            generator.nextId(IdBase.EVENT),
            name,
            description,
            exactInterval,
            newKingdom,
            emptyList(),
            arrayListOf(oldBook)
        )
    }

    fun coronationCelebration() : Event {
        relationships.clear()
        relationships.add(Relationship(
            coronationCelebrationEvent.id,
            coronationTourEvent.id,
            "CAUSED")
        )
        return coronationCelebrationEvent
    }

    fun coronationTour() : Event {
        return coronationTourEvent
    }

    fun kingdomCreated() : Event {
        return coronationKingdomEvent
    }
}