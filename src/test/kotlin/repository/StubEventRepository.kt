package repository

import domain.Event
import repository.identification.IdGenerator
import repository.inmemory.InMemoryEventRepository

class StubEventRepository(
    idGenerator: IdGenerator,
    events: List<Event>
) : InMemoryEventRepository(idGenerator) {

    init {
        events.forEach { insert(it) }
    }
}