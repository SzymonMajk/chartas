package repository

import domain.temporal.Interval
import repository.identification.IdGenerator
import repository.inmemory.InMemoryIntervalRepository

class StubIntervalRepository(
    idGenerator: IdGenerator,
    intervals: List<Interval>
) : InMemoryIntervalRepository(idGenerator) {

    init {
        intervals.forEach { insert(it) }
    }
}