package repository

import domain.spatial.Coordinates
import repository.identification.IdGenerator
import repository.inmemory.InMemoryCoordinatesRepository

class StubCoordinatesRepository(
    idGenerator: IdGenerator,
    coordinates: List<Coordinates>
) : InMemoryCoordinatesRepository(idGenerator) {

    init {
        coordinates.forEach { insert(it) }
    }
}