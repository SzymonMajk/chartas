package repository

import domain.Reference
import repository.identification.IdGenerator
import repository.inmemory.InMemoryReferencesRepository

class StubReferenceRepository(
    idGenerator: IdGenerator,
    references: List<Reference>
) : InMemoryReferencesRepository(idGenerator) {

    init {
        references.forEach { insert(it) }
    }
}