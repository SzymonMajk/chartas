package repository.external

import domain.Reference
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import repository.identification.IdBase
import repository.ReferencesRepository
import repository.external.initialization.Neo4JInitializer
import repository.identification.CountingGenerator

internal class ReferenceRepositoryTest {

    private var generator = CountingGenerator()
    private val repositoryInitializer = Neo4JInitializer(
        generator,
        "bolt://localhost:7687",
        "neo4j",
        "password"
    )
    private lateinit var repository : ReferencesRepository

    @BeforeEach
    private fun initializeRepository() {
        generator = CountingGenerator()
        repository = repositoryInitializer.initializeReferences()
    }

    @Test
    fun referenceWorkflowTest() {
        val nextId = generator.nextId(IdBase.REFERENCE)
        val nextReference = Reference(
            nextId,
            "Old book",
            "Wise man",
            "His own mind")

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextReference))
        assertEquals(nextReference, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }

    @Test
    fun multipleReferencesTest() {
        val firstId = generator.nextId(IdBase.REFERENCE)
        val secondId = generator.nextId(IdBase.REFERENCE)
        val thirdId = generator.nextId(IdBase.REFERENCE)
        val firstReference = Reference(
            firstId,
            "Old book",
            "Wise man",
            "His own mind")
        val secondReference = Reference(
            secondId,
            "Old scroll",
            "Wise man",
            "His own mind")
        val thirdReference = Reference(
            thirdId,
            "Old song",
            "Wise man",
            "His own mind")

        assertEquals(firstId, repository.insert(firstReference))
        assertEquals(secondId, repository.insert(secondReference))
        assertEquals(thirdId, repository.insert(thirdReference))

        val first = listOf(firstReference, secondReference, thirdReference)
        val second = repository.getAll().toList()

        assertTrue(first.size == second.size && first.containsAll(second) && second.containsAll(first))

        assertTrue(repository.remove(firstId))
        assertTrue(repository.remove(secondId))
        assertTrue(repository.remove(thirdId))
    }

    @Test
    fun referenceUpdateTest() {
        val nextId = generator.nextId(IdBase.REFERENCE)
        val nextInterval = Reference(
            nextId,
            "Old book",
            "Wise man",
            "His own mind")

        val updated = Reference(
            nextId,
            "Old book",
            "Wise wife",
            "Real author's mind")

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextInterval))
        assertEquals(updated, repository.replace(updated))
        assertEquals(updated, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }
}