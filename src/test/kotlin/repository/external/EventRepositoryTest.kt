package repository.external

import domain.Event
import domain.Reference
import domain.Relationship
import domain.spatial.Coordinates
import domain.spatial.PointCoordinates
import domain.temporal.ExactInterval
import domain.temporal.Interval
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import repository.*
import repository.external.initialization.Neo4JInitializer
import repository.identification.CountingGenerator
import repository.identification.Id
import repository.identification.IdBase
import java.time.LocalDate

internal class EventRepositoryTest {

    private var generator = CountingGenerator()
    private val repositoryInitializer = Neo4JInitializer(
        generator,
        "bolt://localhost:7687",
        "neo4j",
        "password"
    )
    private lateinit var eventRepository : EventsRepository
    private lateinit var intervalRepository : IntervalsRepository
    private lateinit var coordinatesRepository : CoordinatesRepository
    private lateinit var referencesRepository: ReferencesRepository

    private lateinit var intervalId : Id
    private lateinit var coordinatesId : Id
    private lateinit var exactInterval : Interval
    private lateinit var pointCoordinates : Coordinates
    private lateinit var firstReferenceId : Id
    private lateinit var secondReferenceId : Id
    private lateinit var firstReference : Reference
    private lateinit var secondReference : Reference

    @BeforeEach
    private fun initializeDatabase() {
        generator = CountingGenerator()

        intervalId = generator.nextId(IdBase.INTERVAL)
        exactInterval = ExactInterval(
            intervalId,
            LocalDate.of(1025, 12, 12),
            12
        )

        coordinatesId = generator.nextId(IdBase.COORDINATES)
        pointCoordinates = PointCoordinates(
            coordinatesId,
            arrayOf(49.9, 47.5)
        )

        firstReferenceId = generator.nextId(IdBase.REFERENCE)
        firstReference = Reference(
            firstReferenceId,
            "Old book",
            "Wise man",
            "His own mind"
        )

        secondReferenceId = generator.nextId(IdBase.REFERENCE)
        secondReference = Reference(
            secondReferenceId,
            "New book",
            "John Paul",
            "Unfortunately his own mind"
        )

        eventRepository = repositoryInitializer.initializeEvents()
        intervalRepository = repositoryInitializer.initializeIntervals()
        coordinatesRepository = repositoryInitializer.initializeCoordinates()
        referencesRepository = repositoryInitializer.initializeReferences()

        intervalRepository.insert(exactInterval)
        coordinatesRepository.insert(pointCoordinates)
        referencesRepository.insert(firstReference)
        referencesRepository.insert(secondReference)
    }

    @AfterEach
    private fun clearRepository() {
        intervalRepository.remove(intervalId)
        coordinatesRepository.remove(coordinatesId)
        referencesRepository.remove(firstReferenceId)
        referencesRepository.remove(secondReferenceId)
    }

    @Test
    fun checkIntervalAttachment() {
        val nextId = generator.nextId(IdBase.EVENT)
        val nextEvent = Event(
            nextId,
            "Very old event",
            "No one really no if true!",
            interval = exactInterval
        )

        assertEquals(nextId, eventRepository.insert(nextEvent))
        assertEquals(exactInterval, eventRepository.get(nextId)!!.interval)
        assertEquals(exactInterval, eventRepository.getAll().toList()[0].interval)
        assertTrue(eventRepository.remove(nextId))
    }

    @Test
    fun checkCoordinatesAttachment() {
        val nextId = generator.nextId(IdBase.EVENT)
        val nextEvent = Event(
            nextId,
            "Very old event",
            "No one really no if true!",
            coordinates = pointCoordinates
        )

        assertEquals(nextId, eventRepository.insert(nextEvent))
        assertEquals(pointCoordinates, eventRepository.get(nextId)!!.coordinates)
        assertEquals(pointCoordinates, eventRepository.getAll().toList()[0].coordinates)
        assertTrue(eventRepository.remove(nextId))
    }

    @Test
    fun checkTwoReferencesForOneEvent() {
        val references = listOf(firstReference, secondReference)
        val nextId = generator.nextId(IdBase.REFERENCE)
        val nextEvent = Event(
            nextId,
            "Very old event",
            "No one really no if true!",
            references = references
        )

        assertEquals(nextId, eventRepository.insert(nextEvent))

        val fromSingle = eventRepository.get(nextId)!!.references
        assertTrue(references.size == fromSingle.size &&
                references.containsAll(fromSingle) && fromSingle.containsAll(references))

        val fromMultiple = eventRepository.getAll().toList()[0].references
        assertTrue(references.size == fromMultiple.size &&
                references.containsAll(fromMultiple) && fromMultiple.containsAll(references))

        assertTrue(eventRepository.remove(nextId))
    }

    @Test
    fun relationshipBetweenTwoEvents() {
        val label = "RELATED"
        var outgoingRelationships = emptyList<Relationship>().toMutableList()

        val firstId = generator.nextId(IdBase.EVENT)
        val firstEvent = Event(
            firstId,
            "Cause",
            "Someones's birth!",
            outgoingRelationships = outgoingRelationships
        )

        val secondId = generator.nextId(IdBase.EVENT)
        val secondEvent = Event(
            secondId,
            "Result",
            "Someone's death..."
        )

        val relationship = Relationship(
            firstId,
            secondId,
            label
        )

        outgoingRelationships.add(relationship)

        assertEquals(secondId, eventRepository.insert(secondEvent))
        assertEquals(firstId, eventRepository.insert(firstEvent))

        val fromFirst = eventRepository.get(firstId)!!.outgoingRelationships
        assertEquals(1, fromFirst.size)
        assertEquals(relationship, fromFirst.toList()[0])
        assertTrue(eventRepository.get(secondId)!!.outgoingRelationships.isEmpty())

        assertTrue(eventRepository.remove(firstId))
        assertTrue(eventRepository.remove(secondId))
    }

    @Test
    fun eventWorkflowTest() {
        val nextId = generator.nextId(IdBase.EVENT)
        val nextEvent = Event(
            nextId,
            "Very old event",
            "No one really no if true!"
        )

        assertFalse(eventRepository.remove(nextId))
        assertNull(eventRepository.get(nextId))
        assertEquals(nextId, eventRepository.insert(nextEvent))
        assertEquals(nextEvent, eventRepository.get(nextId))
        assertTrue(eventRepository.remove(nextId))
        assertNull(eventRepository.get(nextId))
        assertFalse(eventRepository.remove(nextId))
    }

    @Test
    fun multipleEventsTest() {
        val firstId = generator.nextId(IdBase.EVENT)
        val secondId = generator.nextId(IdBase.EVENT)
        val thirdId = generator.nextId(IdBase.EVENT)
        val firstEvent = Event(
            firstId,
            "Very old event",
            "No one really no if true!")
        val secondEvent = Event(
            secondId,
            "Even older one!",
            "No one really no if true!")
        val thirdEvent = Event(
            thirdId,
            "Latest news",
            "No one really no if true!")

        assertEquals(firstId, eventRepository.insert(firstEvent))
        assertEquals(secondId, eventRepository.insert(secondEvent))
        assertEquals(thirdId, eventRepository.insert(thirdEvent))

        val first = listOf(firstEvent, secondEvent, thirdEvent)
        val second = eventRepository.getAll().toList()

        assertTrue(first.size == second.size && first.containsAll(second) && second.containsAll(first))

        assertTrue(eventRepository.remove(firstId))
        assertTrue(eventRepository.remove(secondId))
        assertTrue(eventRepository.remove(thirdId))
    }

    @Test
    fun eventUpdateTest() {
        val nextId = generator.nextId(IdBase.EVENT)
        val nextEvent = Event(
            nextId,
            "Very old event",
            "No one really no if true!"
        )

        val updated = Event(
            nextId,
            "Latest news",
            "No one really no if true!"
        )

        assertFalse(eventRepository.remove(nextId))
        assertNull(eventRepository.get(nextId))
        assertEquals(nextId, eventRepository.insert(nextEvent))
        assertEquals(updated, eventRepository.replace(updated))
        assertEquals(updated, eventRepository.get(nextId))
        assertTrue(eventRepository.remove(nextId))
        assertNull(eventRepository.get(nextId))
        assertFalse(eventRepository.remove(nextId))
    }
}