package repository.external

import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import repository.identification.IdBase
import repository.IntervalsRepository
import repository.external.initialization.Neo4JInitializer
import repository.identification.CountingGenerator
import java.time.LocalDate

internal class IntervalRepositoryTest {

    private var generator = CountingGenerator()
    private val repositoryInitializer = Neo4JInitializer(
        generator,
        "bolt://localhost:7687",
        "neo4j",
        "password"
    )
    private lateinit var repository : IntervalsRepository

    @BeforeEach
    private fun initializeRepository() {
        generator = CountingGenerator()
        repository = repositoryInitializer.initializeIntervals()
    }

    @Test
    fun intervalWorkflowTest() {
        val nextId = generator.nextId(IdBase.INTERVAL)
        val nextInterval = ExactInterval(
            nextId,
            LocalDate.of(1025, 12, 12),
            12
        )

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextInterval))
        assertEquals(nextInterval, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }

    @Test
    fun multipleIntervalsTest() {
        val firstId = generator.nextId(IdBase.INTERVAL)
        val secondId = generator.nextId(IdBase.INTERVAL)
        val thirdId = generator.nextId(IdBase.INTERVAL)
        val firstInterval = ExactInterval(
            firstId,
            LocalDate.of(1025, 12, 12),
            12
        )
        val secondInterval = FuzzyInterval(
            secondId,
            LocalDate.of(1027, 12, 12),
            13,
            2,
            3
        )
        val thirdInterval = ExactInterval(
            thirdId,
            LocalDate.of(1029, 12, 12),
            14
        )

        assertEquals(firstId, repository.insert(firstInterval))
        assertEquals(secondId, repository.insert(secondInterval))
        assertEquals(thirdId, repository.insert(thirdInterval))

        val first = listOf(firstInterval, secondInterval, thirdInterval)
        val second = repository.getAll().toList()

        assertTrue(first.size == second.size && first.containsAll(second) && second.containsAll(first))

        assertTrue(repository.remove(firstId))
        assertTrue(repository.remove(secondId))
        assertTrue(repository.remove(thirdId))
    }

    @Test
    fun intervalUpdateTest() {
        val nextId = generator.nextId(IdBase.INTERVAL)
        val nextInterval = ExactInterval(
            nextId,
            LocalDate.of(1025, 12, 12),
            12)

        val updated = FuzzyInterval(
            nextId,
            LocalDate.of(1029, 12, 14),
            14,
            2,
            4
        )

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextInterval))
        assertEquals(updated, repository.replace(updated))
        assertEquals(updated, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }
}