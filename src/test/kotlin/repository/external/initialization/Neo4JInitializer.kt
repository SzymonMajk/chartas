package repository.external.initialization

import repository.external.neo4j.Neo4JCoordinatesRepository
import repository.external.neo4j.Neo4JEventRepository
import repository.external.neo4j.Neo4JIntervalRepository
import repository.external.neo4j.Neo4JReferenceRepository
import repository.identification.IdGenerator

class Neo4JInitializer(
    private val generator: IdGenerator,
    private val uri : String,
    private val user : String,
    private val password : String
) : RepositoryInitializer {

    override fun initializeEvents() =
        Neo4JEventRepository(
            generator,
            uri,
            user,
            password
        )

    override fun initializeIntervals() =
        Neo4JIntervalRepository(
            generator,
            uri,
            user,
            password
        )

    override fun initializeCoordinates() =
        Neo4JCoordinatesRepository(
            generator,
            uri,
            user,
            password
        )

    override fun initializeReferences() =
        Neo4JReferenceRepository(
            generator,
            uri,
            user,
            password
        )
}