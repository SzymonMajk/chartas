package repository.external.initialization

import repository.CoordinatesRepository
import repository.EventsRepository
import repository.IntervalsRepository
import repository.ReferencesRepository

interface RepositoryInitializer {

    fun initializeEvents() : EventsRepository

    fun initializeIntervals() : IntervalsRepository

    fun initializeCoordinates() : CoordinatesRepository

    fun initializeReferences() : ReferencesRepository
}