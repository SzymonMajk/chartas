package repository.external

import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import repository.CoordinatesRepository
import repository.identification.IdBase
import repository.external.initialization.Neo4JInitializer
import repository.identification.CountingGenerator

internal class CoordinatesRepositoryTest {

    private var generator = CountingGenerator()
    private val repositoryInitializer = Neo4JInitializer(
        generator,
        "bolt://localhost:7687",
        "neo4j",
        "password"
    )
    private lateinit var repository : CoordinatesRepository

    @BeforeEach
    private fun initializeRepository() {
        generator = CountingGenerator()
        repository = repositoryInitializer.initializeCoordinates()
    }

    @Test
    fun coordinatesWorkflowTest() {
        val nextId = generator.nextId(IdBase.COORDINATES)
        val nextCoordinates = PointCoordinates(
            nextId,
            arrayOf(49.9, 47.5)
        )

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextCoordinates))
        assertEquals(nextCoordinates, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }

    @Test
    fun multipleCoordinatesTest() {
        val firstId = generator.nextId(IdBase.COORDINATES)
        val secondId = generator.nextId(IdBase.COORDINATES)
        val thirdId = generator.nextId(IdBase.COORDINATES)
        val firstCoordinates = PointCoordinates(
            firstId,
            arrayOf(49.9, 47.5)
        )
        val secondCoordinates = LineCoordinates(
            secondId,
            arrayOf(
                arrayOf(49.9, 47.5),
                arrayOf(49.9, 46.5),
                arrayOf(48.8, 47.5))
        )
        val thirdCoordinates = PolygonCoordinates(
            thirdId,
            arrayOf(arrayOf(
                arrayOf(50.2,48.2),
                arrayOf(50.2, 51.2),
                arrayOf(47.3,51.2),
                arrayOf(47.3, 48.2)))
        )

        assertEquals(firstId, repository.insert(firstCoordinates))
        assertEquals(secondId, repository.insert(secondCoordinates))
        assertEquals(thirdId, repository.insert(thirdCoordinates))

        val first = listOf(firstCoordinates, secondCoordinates, thirdCoordinates)
        val second = repository.getAll().toList()

        assertTrue(first.size == second.size && first.containsAll(second) && second.containsAll(first))

        assertTrue(repository.remove(firstId))
        assertTrue(repository.remove(secondId))
        assertTrue(repository.remove(thirdId))
    }

    @Test
    fun coordinatesUpdateTest() {
        val nextId = generator.nextId(IdBase.COORDINATES)
        val nextCoordinates = PointCoordinates(
            nextId,
            arrayOf(49.9, 47.5)
        )

        val updated = PolygonCoordinates(
            nextId,
            arrayOf(arrayOf(
                arrayOf(50.2,48.2),
                arrayOf(50.2, 51.2),
                arrayOf(47.3,51.2),
                arrayOf(47.3, 48.2))
            )
        )

        assertFalse(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertEquals(nextId, repository.insert(nextCoordinates))
        assertEquals(updated, repository.replace(updated))
        assertEquals(updated, repository.get(nextId))
        assertTrue(repository.remove(nextId))
        assertNull(repository.get(nextId))
        assertFalse(repository.remove(nextId))
    }
}