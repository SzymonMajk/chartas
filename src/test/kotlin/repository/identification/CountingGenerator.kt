package repository.identification

import java.util.concurrent.atomic.AtomicLong

class CountingGenerator : IdGenerator {

    private val counter = AtomicLong(0)

    override fun nextId(idBase: IdBase): Id {
        return Id("test", counter.incrementAndGet())
    }

}