package commands.connect

import domain.Event
import domain.temporal.ExactInterval
import domain.temporal.Interval
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import repository.identification.Id

import repository.StubEventRepository
import repository.StubIntervalRepository
import repository.identification.CountingGenerator
import java.time.LocalDate
import java.time.Month
import kotlin.test.assertTrue

internal class IntervalConnectionsTest {

    private val generator = CountingGenerator()
    private val eventId = Id("Event:1")
    private val intervalId = Id("Interval:1")

    private var event : Event? = null
    private var interval : Interval? = null

    private fun resetDisconnected() {
        event = Event(
            eventId,
            "test",
            "test")
        interval = ExactInterval(
            intervalId, LocalDate.of(1025, Month.APRIL, 19),
            2
        )
    }

    private fun resetConnected() {
        interval = ExactInterval(
            intervalId, LocalDate.of(1025, Month.APRIL, 19),
            2
        )
        event = Event(
            eventId,
            "test",
            "test",
            interval)
    }

    @Test
    fun shouldIndicateWhenIntervalDetached() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val intervalRepository = StubIntervalRepository(generator, listOf(interval as Interval))
        val command = CheckIntervalCommand(eventsRepository, intervalRepository, eventId, intervalId)

        command.execute()

        assertFalse(command.getResult())
    }

    @Test
    fun shouldIndicateWhenIntervalAttached() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val intervalRepository = StubIntervalRepository(generator, listOf(interval as Interval))
        val command = CheckIntervalCommand(eventsRepository, intervalRepository, eventId, intervalId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldAttachCorrectly() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val intervalRepository = StubIntervalRepository(generator, listOf(interval as Interval))
        val command = AttachIntervalCommand(eventsRepository, intervalRepository, eventId, intervalId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldDetachCorrectly() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val intervalRepository = StubIntervalRepository(generator, listOf(interval as Interval))
        val command = DetachIntervalCommand(eventsRepository, intervalRepository, eventId, intervalId)

        command.execute()

        assertTrue(command.getResult())
    }
}