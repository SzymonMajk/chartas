package commands.connect

import domain.Event
import domain.Reference
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import repository.identification.Id
import repository.StubEventRepository
import repository.StubReferenceRepository
import repository.identification.CountingGenerator
import kotlin.test.assertTrue

internal class ReferencesConnectionsTest {

    private val generator = CountingGenerator()
    private val eventId = Id("Event:1")
    private val referencesId = Id("Reference:1")

    private var event : Event? = null
    private var reference : Reference? = null

    private fun resetDisconnected() {
        event = Event(
            eventId,
            "test",
            "test")
        reference = Reference(
            referencesId,
            "test",
            "test",
            "test"
        )
    }

    private fun resetConnected() {
        reference = Reference(
            referencesId,
            "test",
            "test",
            "test"
        )
        event = Event(
            eventId,
            "test",
            "test",
            null,
            null,
            emptyList(),
            listOf(reference as Reference)
        )
    }

    @Test
    fun shouldIndicateWhenIntervalDetached() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val referenceRepository = StubReferenceRepository(generator, listOf(reference as Reference))
        val command = CheckReferenceCommand(eventsRepository, referenceRepository, eventId, referencesId)

        command.execute()

        assertFalse(command.getResult())
    }

    @Test
    fun shouldIndicateWhenIntervalAttached() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val referenceRepository = StubReferenceRepository(generator, listOf(reference as Reference))
        val command = CheckReferenceCommand(eventsRepository, referenceRepository, eventId, referencesId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldAttachCorrectly() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val referenceRepository = StubReferenceRepository(generator, listOf(reference as Reference))
        val command = AttachReferenceCommand(eventsRepository, referenceRepository, eventId, referencesId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldDetachCorrectly() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val referenceRepository = StubReferenceRepository(generator, listOf(reference as Reference))
        val command = DetachReferenceCommand(eventsRepository, referenceRepository, eventId, referencesId)

        command.execute()

        assertTrue(command.getResult())
    }
}