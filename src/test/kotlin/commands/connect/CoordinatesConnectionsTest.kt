package commands.connect

import commands.connect.AttachCoordinatesCommand
import commands.connect.CheckCoordinatesCommand
import commands.connect.DetachCoordinatesCommand
import domain.Event
import domain.spatial.Coordinates
import domain.spatial.PointCoordinates
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import repository.identification.Id
import repository.StubCoordinatesRepository
import repository.StubEventRepository
import repository.identification.CountingGenerator
import kotlin.test.assertTrue

internal class CoordinatesConnectionsTest {

    private val generator = CountingGenerator()
    private val eventId = Id("Event:1")
    private val coordinatesId = Id("Coordinates:1")

    private var event : Event? = null
    private var coordinates : Coordinates? = null

    private fun resetDisconnected() {
        event = Event(
            eventId,
            "test",
            "test")
        coordinates = PointCoordinates(
            coordinatesId,
            arrayOf(12.3,25.2)
        )
    }

    private fun resetConnected() {
        coordinates = PointCoordinates(
            coordinatesId,
            arrayOf(12.3,25.2)
        )
        event = Event(
            eventId,
            "test",
            "test",
            null,
            coordinates)
    }

    @Test
    fun shouldIndicateWhenCoordinatesDetached() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val coordinatesRepository = StubCoordinatesRepository(generator, listOf(coordinates as Coordinates))
        val command = CheckCoordinatesCommand(eventsRepository, coordinatesRepository, eventId, coordinatesId)

        command.execute()

        assertFalse(command.getResult())
    }

    @Test
    fun shouldIndicateWhenCoordinatesAttached() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val coordinatesRepository = StubCoordinatesRepository(generator, listOf(coordinates as Coordinates))
        val command = CheckCoordinatesCommand(eventsRepository, coordinatesRepository, eventId, coordinatesId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldAttachCorrectly() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val coordinatesRepository = StubCoordinatesRepository(generator, listOf(coordinates as Coordinates))
        val command = AttachCoordinatesCommand(eventsRepository, coordinatesRepository, eventId, coordinatesId)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldDetachCorrectly() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val coordinatesRepository = StubCoordinatesRepository(generator, listOf(coordinates as Coordinates))
        val command = DetachCoordinatesCommand(eventsRepository, coordinatesRepository, eventId, coordinatesId)

        command.execute()

        assertTrue(command.getResult())
    }
}