package commands.connect

import commands.search.DrainOptimizerCommand
import commands.search.FillOptimizerCommand
import domain.Event
import domain.temporal.ExactInterval
import domain.temporal.Interval
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import repository.identification.Id

import repository.StubEventRepository
import repository.identification.CountingGenerator
import search.SequentialOptimizer
import java.time.LocalDate
import java.time.Month
import kotlin.test.assertEquals

internal class OptimizationReplicationTest {

    private var optimizer = SequentialOptimizer()
    private val generator = CountingGenerator()
    private val eventId = Id("Event:1")
    private val intervalId = Id("Interval:1")

    private var event : Event? = null
    private var interval : Interval? = null

    private fun resetDisconnected() {
        event = Event(
            eventId,
            "test",
            "test")
        interval = ExactInterval(
            intervalId, LocalDate.of(1025, Month.APRIL, 19),
            2
        )
    }

    private fun resetConnected() {
        interval = ExactInterval(
            intervalId, LocalDate.of(1025, Month.APRIL, 19),
            2
        )
        event = Event(
            eventId,
            "test",
            "test",
            interval)
    }

    @BeforeEach
    private fun resetOptimizer() {
        optimizer = SequentialOptimizer()
    }

    @Test
    fun shouldCorrectlyAddEventToOptimizer() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val command = FillOptimizerCommand(eventsRepository, optimizer, eventId)

        command.execute()

        assertEquals(event, optimizer.getVisible(
            LocalDate.of(1025, Month.APRIL, 15),
            LocalDate.of(1025, Month.APRIL, 25)).toList()[0])
    }

    @Test
    fun shouldCorrectlyRemoveEventFromOptimizer() {
        resetDisconnected()
        optimizer.add(event!!)
        val eventsRepository = StubEventRepository(generator, listOf(event as Event))
        val command = DrainOptimizerCommand(eventsRepository, optimizer, eventId)

        command.execute()

        assertEquals(0, optimizer.getVisible(
            LocalDate.of(1025, Month.APRIL, 15),
            LocalDate.of(1025, Month.APRIL, 25)).size)
    }
}