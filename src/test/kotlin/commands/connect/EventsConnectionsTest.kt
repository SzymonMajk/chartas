package commands.connect

import domain.Event
import domain.Relationship
import org.junit.jupiter.api.Test
import repository.identification.Id
import repository.StubEventRepository
import repository.identification.CountingGenerator
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class EventsConnectionsTest {

    private val generator = CountingGenerator()
    private val firstId = Id("Event:1")
    private val secondId = Id("Event:2")

    private var first : Event? = null
    private var second : Event? = null
    private val label = "TestLabel"


    private fun resetDisconnected() {
        first = Event(
            firstId,
            "first",
            "test")

        second = Event(
            secondId,
            "second",
            "test")
    }

    private fun resetConnected() {
        first = Event(
            firstId,
            "first",
            "test"
        )

        second = Event(
            secondId,
            "second",
            "test")

        val relationship = Relationship(first!!.id, second!!.id, label)
        (first as Event).outgoingRelationships = (first as Event).outgoingRelationships.plus(relationship)
    }

    @Test
    fun shouldIndicateWhenEventsDisconnected() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(first as Event, second as Event))
        val command = CheckEventsCommand(eventsRepository, firstId, secondId)

        command.execute()

        assertTrue(command.getResult() == null)
    }

    @Test
    fun shouldIndicateWhenEventsConnected() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(first as Event, second as Event))
        val command = CheckEventsCommand(eventsRepository, firstId, secondId)

        command.execute()

        assertEquals(label, command.getResult())
    }

    @Test
    fun shouldConnectCorrectly() {
        resetDisconnected()
        val eventsRepository = StubEventRepository(generator, listOf(first as Event, second as Event))
        val command = AttachEventsCommand(eventsRepository, firstId, secondId, label)

        command.execute()

        assertTrue(command.getResult())
    }

    @Test
    fun shouldDisconnectCorrectly() {
        resetConnected()
        val eventsRepository = StubEventRepository(generator, listOf(first as Event, second as Event))
        val command = DetachEventsCommand(eventsRepository, firstId, secondId, label)

        command.execute()

        assertTrue(command.getResult())
    }
}