package export

import domain.Event
import domain.Reference
import domain.Relationship
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONArray
import org.json.JSONObject
import repository.identification.Id

class GraphJsonExporter : ExportVisitor {

    private var currentEventId = ""
    private var intervalNodesIds = emptySet<Id>()
    private var coordinatesNodesIds = emptySet<Id>()
    private var referencesNodesIds = emptySet<Id>()

    override fun serializeEvent(event: Event, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()

        node.put("label","Event")
        node.put("name", event.name)
        node.put("description", event.description)
        node.put("id", event.id)

        if (!base.has("nodes")) {
            base.put("nodes", JSONArray())
        }

        base.getJSONArray("nodes").put(node)

        if (!base.has("edges")) {
            base.put("edges", JSONArray())
        }

        currentEventId = event.id.toString()

        return base.toString()
    }

    override fun serializeInterval(interval: ExactInterval, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!intervalNodesIds.contains(interval.id)) {
            node.put("label", "Interval")
            node.put("id", interval.id)
            node.put("start", interval.date)
            node.put("end", interval.date.plusDays(interval.duration.toLong()))
            base.getJSONArray("nodes").put(node)
            intervalNodesIds = intervalNodesIds.plus(interval.id)
        }

        edge.put("label", "WHEN")
        edge.put("to", interval.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeInterval(interval: FuzzyInterval, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!intervalNodesIds.contains(interval.id)) {
            node.put("label", "Interval")
            node.put("id", interval.id)
            node.put("start", interval.date)
            node.put("end", interval.date.plusDays(interval.duration.toLong()))
            node.put("commenceUncertainty", interval.commenceUncertainty)
            node.put("terminationUncertainty", interval.terminationUncertainty)
            base.getJSONArray("nodes").put(node)
            intervalNodesIds = intervalNodesIds.plus(interval.id)
        }
        edge.put("label", "WHEN")
        edge.put("to", interval.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PointCoordinates, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!coordinatesNodesIds.contains(coordinates.id)) {
            node.put("label", "Coordinates")
            node.put("id", coordinates.id)
            node.put("coordinates", coordinates.lonLat)
            coordinatesNodesIds = coordinatesNodesIds.plus(coordinates.id)
            base.getJSONArray("nodes").put(node)
        }

        edge.put("label", "WHERE")
        edge.put("to", coordinates.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: LineCoordinates, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!coordinatesNodesIds.contains(coordinates.id)) {
            node.put("label", "Coordinates")
            node.put("id", coordinates.id)
            node.put("coordinates", coordinates.points)
            base.getJSONArray("nodes").put(node)
            coordinatesNodesIds = coordinatesNodesIds.plus(coordinates.id)
        }
        edge.put("label", "WHERE")
        edge.put("to", coordinates.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PolygonCoordinates, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!coordinatesNodesIds.contains(coordinates.id)) {
            node.put("label", "Coordinates")
            node.put("id", coordinates.id)
            node.put("coordinates", coordinates.polygons)
            base.getJSONArray("nodes").put(node)
            coordinatesNodesIds = coordinatesNodesIds.plus(coordinates.id)
        }
        edge.put("label", "WHERE")
        edge.put("to", coordinates.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeReference(reference: Reference, state: String): String {
        val base = JSONObject(state)
        val node = JSONObject()
        val edge = JSONObject()

        if (!coordinatesNodesIds.contains(reference.id)) {
            node.put("label", "Reference")
            node.put("id", reference.id)
            node.put("title", reference.title)
            node.put("author", reference.author)
            node.put("source", reference.source)
            base.getJSONArray("nodes").put(node)
            coordinatesNodesIds = coordinatesNodesIds.plus(reference.id)
        }
        edge.put("label", "REFERENCED")
        edge.put("to", reference.id.toString())
        edge.put("from", currentEventId)
        base.getJSONArray("edges").put(edge)

        return base.toString()
    }

    override fun serializeRelationship(relationship: Relationship, state: String): String {
        val base = JSONObject(state)
        val edge = JSONObject()

        edge.put("label", relationship.label)
        edge.put("from", relationship.to)
        edge.put("to", relationship.from)

        base.getJSONArray("edges").put(edge)

        return base.toString()
    }
}