package export

import domain.Event
import domain.Relationship
import domain.Reference
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONArray
import org.json.JSONObject

class GeoJsonExporter : ExportVisitor {

    private fun getLastFeatureProperties(base : JSONObject) : JSONObject {
        val features = base.getJSONArray("features")
        val lastFeature = features.getJSONObject(features.length() - 1)
        return lastFeature.getJSONObject("properties")
    }

    private fun getLastFeatureGeometry(base : JSONObject) : JSONObject {
        val features = base.getJSONArray("features")
        val currentFeatures = features.getJSONObject(features.length() - 1)
        return currentFeatures.getJSONObject("geometry")
    }

    override fun serializeEvent(event: Event, state : String) : String {
        val base = JSONObject(state)
        val newFeature = JSONObject()
        val properties = JSONObject()

        properties.put("name", event.name)
        properties.put("description", event.description)
        properties.put("id", event.id)
        properties.put("references", JSONArray())

        newFeature.put("type", "Feature")
        newFeature.put("properties", properties)
        newFeature.put("geometry", JSONObject())

        if (!base.has("type")) {
            base.put("type","FeatureCollection")
        }

        if (!base.has("features")) {
            base.put("features", JSONArray())
        }

        base.getJSONArray("features").put(newFeature)

        return base.toString()
    }

    override fun serializeInterval(interval: ExactInterval, state: String): String {
        val base = JSONObject(state)
        val properties = getLastFeatureProperties(base)

        properties.put("date", interval.date)
        properties.put("duration", interval.duration)

        return base.toString()
    }

    override fun serializeInterval(interval: FuzzyInterval, state : String) : String {
        val base = JSONObject(state)
        val properties = getLastFeatureProperties(base)

        properties.put("date", interval.date)
        properties.put("duration", interval.duration)
        properties.put("commenceUncertainty", interval.commenceUncertainty)
        properties.put("terminationUncertainty", interval.terminationUncertainty)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PointCoordinates, state : String) : String {
        val base = JSONObject(state)
        val geometry = getLastFeatureGeometry(base)

        geometry.put("type", "Point")
        geometry.put("id", coordinates.id)
        geometry.put("coordinates", coordinates.lonLat)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: LineCoordinates, state : String) : String {
        val base = JSONObject(state)
        val geometry = getLastFeatureGeometry(base)

        geometry.put("type", "LineString")
        geometry.put("id", coordinates.id)
        geometry.put("coordinates", coordinates.points)

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PolygonCoordinates, state : String) : String {
        val base = JSONObject(state)
        val geometry = getLastFeatureGeometry(base)

        geometry.put("type", "Polygon")
        geometry.put("id", coordinates.id)
        geometry.put("coordinates", coordinates.polygons)

        return base.toString()
    }

    override fun serializeReference(reference: Reference, state : String) : String {
        val base = JSONObject(state)
        val properties = getLastFeatureProperties(base)

        properties.getJSONArray("references").put(reference.title)

        return base.toString()
    }

    override fun serializeRelationship(relationship: Relationship, state : String) : String {
        return state
    }
}