package export

import domain.Event
import domain.Relationship
import domain.temporal.Interval
import domain.Reference
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval

interface ExportVisitor {

    fun serializeEvent(event : Event, state : String) : String

    fun serializeInterval(interval : ExactInterval, state : String) : String

    fun serializeInterval(interval : FuzzyInterval, state : String) : String

    fun serializeCoordinates(coordinates: PointCoordinates, state : String) : String

    fun serializeCoordinates(coordinates: LineCoordinates, state : String) : String

    fun serializeCoordinates(coordinates: PolygonCoordinates, state : String) : String

    fun serializeReference(reference: Reference, state : String) : String

    fun serializeRelationship(relationship: Relationship, state : String) : String
}