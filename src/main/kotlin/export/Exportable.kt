package export


interface Exportable {
    fun accept(visitor : ExportVisitor, state : String) : String
}