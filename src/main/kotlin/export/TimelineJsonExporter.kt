package export

import domain.Event
import domain.Reference
import domain.Relationship
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONArray
import org.json.JSONObject


class TimelineJsonExporter : ExportVisitor {
    override fun serializeEvent(event: Event, state: String): String {
        val base = JSONObject(state)
        val entity = JSONObject()

        entity.put("id", event.id)
        entity.put("content", event.name)
        entity.put("description", event.description)
        entity.put("references", JSONArray())

        if (!base.has("events")) {
            base.put("events", JSONArray())
        }

        base.getJSONArray("events").put(entity)

        return base.toString()
    }

    override fun serializeInterval(interval: ExactInterval, state: String): String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)

        lastEntity.put("start", interval.date)
        lastEntity.put("end", interval.date.plusDays(interval.duration.toLong()))

        return base.toString()
    }

    override fun serializeInterval(interval: FuzzyInterval, state: String): String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)

        lastEntity.put("start", interval.date.minusDays(interval.commenceUncertainty.toLong()))
        lastEntity.put("end", interval.date.plusDays((interval.duration + interval.terminationUncertainty).toLong()))

        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PointCoordinates, state: String): String {
        return state
    }

    override fun serializeCoordinates(coordinates: LineCoordinates, state: String): String {
        return state
    }

    override fun serializeCoordinates(coordinates: PolygonCoordinates, state: String): String {
        return state
    }

    override fun serializeReference(reference: Reference, state: String): String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)

        lastEntity.getJSONArray("references").put(reference.title)

        return base.toString()
    }

    override fun serializeRelationship(relationship: Relationship, state: String): String {
        return state
    }
}