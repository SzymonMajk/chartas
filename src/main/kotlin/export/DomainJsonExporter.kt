package export

import domain.Event
import domain.Relationship
import domain.Reference
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONArray
import org.json.JSONObject

class DomainJsonExporter : ExportVisitor {

    override fun serializeEvent(event: Event, state : String) : String {
        val base = JSONObject(state)
        val entity = JSONObject()

        entity.put("name", event.name)
        entity.put("description", event.description)
        entity.put("id", event.id)
        entity.put("references", JSONArray())
        entity.put("relationships", JSONArray())

        if (!base.has("events")) {
            base.put("events", JSONArray())
        }

        base.getJSONArray("events").put(entity)

        return base.toString()
    }

    override fun serializeInterval(interval: ExactInterval, state: String): String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.put("intervalId", interval.id)
        return base.toString()
    }

    override fun serializeInterval(interval: FuzzyInterval, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.put("intervalId", interval.id)
        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PointCoordinates, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.put("coordinatesId", coordinates.id)
        return base.toString()
    }

    override fun serializeCoordinates(coordinates: LineCoordinates, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.put("coordinatesId", coordinates.id)
        return base.toString()
    }

    override fun serializeCoordinates(coordinates: PolygonCoordinates, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.put("coordinatesId", coordinates.id)
        return base.toString()
    }

    override fun serializeReference(reference: Reference, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.getJSONArray("references").put(reference.id)
        return base.toString()
    }

    override fun serializeRelationship(relationship: Relationship, state : String) : String {
        val base = JSONObject(state)
        val events = base.getJSONArray("events")
        val lastEntity = events.getJSONObject(events.length() - 1)
        lastEntity.getJSONArray("relationships").put(relationship.to)
        return base.toString()
    }
}