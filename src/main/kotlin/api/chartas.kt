@file:JvmName("Chartas")

package api

import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.*
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.*
import io.ktor.jackson.jackson
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.KtorExperimentalAPI
import org.koin.core.module.Module
import repository.inmemory.InMemoryCoordinatesRepository
import repository.inmemory.InMemoryEventRepository
import repository.inmemory.InMemoryIntervalRepository
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import repository.*
import repository.external.neo4j.Neo4JCoordinatesRepository
import repository.external.neo4j.Neo4JEventRepository
import repository.external.neo4j.Neo4JIntervalRepository
import repository.external.neo4j.Neo4JReferenceRepository
import repository.identification.ApplicationIdGenerator
import repository.identification.IdGenerator
import repository.inmemory.InMemoryReferencesRepository
import search.SearchOptimizer
import search.SequentialOptimizer
import search.bisection.BisectionOptimizer
import search.clustering.IntervalClusteringOptimizer
import search.tree.IntervalTreeOptimizer
import java.io.File

@KtorExperimentalAPI
fun main(args: Array<String>) {
    val server = embeddedServer(Netty, port = 8080) {
        install(StatusPages) {
            exception<Throwable> { e ->
                call.respondText(e.localizedMessage,ContentType.Text.Plain, HttpStatusCode.InternalServerError)
            }
        }
        install(ContentNegotiation) {
            jackson {
                // extension method of ObjectMapper to allow config etc
                enable(SerializationFeature.INDENT_OUTPUT)
            }
        }
        install(CORS)
        {
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Put)
            method(HttpMethod.Delete)
            header(HttpHeaders.AccessControlAllowHeaders)
            header(HttpHeaders.ContentType)
            header(HttpHeaders.AccessControlAllowOrigin)
            allowCredentials = true
            anyHost()
        }
        install(Koin) {
            modules(configureModules())
        }
        routing {
            invitation()
            events()
            intervals()
            coordinates()
            references()
            searching()
        }
    }
    server.start(wait = true)
}

fun configureModules(

) : Module {
    val configuration = object {}.javaClass.classLoader.getResource("application.conf").readText().split("\n")
    val parsedConfiguration = configuration
        .filter { !it.contains(Regex("[{}$]")) && it.isNotBlank() }
        .map { it.substring(it.indexOf("=") + 2)}
        .map { it.replace("\"","")}

    val optimizationConfig = parsedConfiguration[0]
    val persistenceConfig =  parsedConfiguration[1]
    val persistenceAddress =  parsedConfiguration[2]
    val persistenceUser =  parsedConfiguration[3]
    val persistencePassword =  parsedConfiguration[4]

    return when {
        persistenceConfig == "neo4j" && optimizationConfig == "tree"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { Neo4JEventRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<IntervalsRepository> { Neo4JIntervalRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<CoordinatesRepository> { Neo4JCoordinatesRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<ReferencesRepository> { Neo4JReferenceRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<SearchOptimizer> { IntervalTreeOptimizer() }
            }
        }
        persistenceConfig == "neo4j" && optimizationConfig == "bisection"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { Neo4JEventRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<IntervalsRepository> { Neo4JIntervalRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<CoordinatesRepository> { Neo4JCoordinatesRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<ReferencesRepository> { Neo4JReferenceRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<SearchOptimizer> { BisectionOptimizer() }
            }
        }
        persistenceConfig == "neo4j" && optimizationConfig == "clustering"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { Neo4JEventRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<IntervalsRepository> { Neo4JIntervalRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<CoordinatesRepository> { Neo4JCoordinatesRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<ReferencesRepository> { Neo4JReferenceRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<SearchOptimizer> { IntervalClusteringOptimizer() }
            }
        }
        persistenceConfig == "neo4j" -> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { Neo4JEventRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<IntervalsRepository> { Neo4JIntervalRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<CoordinatesRepository> { Neo4JCoordinatesRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<ReferencesRepository> { Neo4JReferenceRepository(get(), persistenceAddress, persistenceUser, persistencePassword) }
                single<SearchOptimizer> { SequentialOptimizer() }
            }
        }
        optimizationConfig == "tree"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { InMemoryEventRepository(get()) }
                single<IntervalsRepository> { InMemoryIntervalRepository(get()) }
                single<CoordinatesRepository> { InMemoryCoordinatesRepository(get()) }
                single<ReferencesRepository> { InMemoryReferencesRepository(get()) }
                single<SearchOptimizer> { IntervalTreeOptimizer() }
            }
        }
        optimizationConfig == "bisection"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { InMemoryEventRepository(get()) }
                single<IntervalsRepository> { InMemoryIntervalRepository(get()) }
                single<CoordinatesRepository> { InMemoryCoordinatesRepository(get()) }
                single<ReferencesRepository> { InMemoryReferencesRepository(get()) }
                single<SearchOptimizer> { BisectionOptimizer() }
            }
        }
        optimizationConfig == "clustering"-> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { InMemoryEventRepository(get()) }
                single<IntervalsRepository> { InMemoryIntervalRepository(get()) }
                single<CoordinatesRepository> { InMemoryCoordinatesRepository(get()) }
                single<ReferencesRepository> { InMemoryReferencesRepository(get()) }
                single<SearchOptimizer> { IntervalClusteringOptimizer() }
            }
        }
        else -> {
            module {
                single<IdGenerator> { ApplicationIdGenerator() }
                single<EventsRepository> { InMemoryEventRepository(get()) }
                single<IntervalsRepository> { InMemoryIntervalRepository(get()) }
                single<CoordinatesRepository> { InMemoryCoordinatesRepository(get()) }
                single<ReferencesRepository> { InMemoryReferencesRepository(get()) }
                single<SearchOptimizer> { SequentialOptimizer() }
            }
        }
    }
}