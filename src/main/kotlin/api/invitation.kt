package api

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Routing.invitation() {
    get("/") {
        call.respond(HttpStatusCode.OK, "Welcome to chartas!")
    }
}