package api

import commands.search.ExportCommand
import commands.search.SearchCommand
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import org.koin.ktor.ext.inject
import search.SearchOptimizer
import java.time.LocalDate

fun Routing.searching() {
    val optimizer by inject<SearchOptimizer>()

    get("/exporters/{format}") {
        val queryParameters: Parameters = call.request.queryParameters
        val format = call.parameters["format"]
        val threshold = call.parameters["threshold"]

        if (queryParameters.contains("start") && queryParameters.contains("end")) {
            val start = LocalDate.parse(queryParameters["start"])
            val end = LocalDate.parse(queryParameters["end"])

            if (start != null && end != null) {
                val search = SearchCommand(optimizer, start, end, threshold)
                search.execute()
                val searchResults = search.getResult()

                val export = ExportCommand(format, searchResults)
                export.execute()
                val result = export.getResult()

                if (result != null) {
                    call.respond(HttpStatusCode.OK, result)
                } else {
                    call.respond(HttpStatusCode.BadRequest, "Wrong format provided")
                }
            } else {
                call.respond(
                    HttpStatusCode.BadRequest,
                    "Select one of the formats and provide as path parametes [chronology, geojson, graphjson]"
                )
            }
        } else {
            call.respond(
                HttpStatusCode.BadRequest,
                "Start and End parameters should be correct dates."
            )
        }
    }
}