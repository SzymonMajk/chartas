package api

import commands.Status
import commands.connect.AttachEventsCommand
import commands.connect.CheckEventsCommand
import commands.create.CreateEventCommand
import commands.delete.DeleteEventCommand
import commands.read.ReadEventCommand
import commands.read.ReadEventsCommand
import commands.update.UpdateEventCommand
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.*
import org.json.JSONObject
import org.koin.ktor.ext.inject
import repository.*
import repository.identification.Id

fun Routing.events() {
    val eventRepository by inject<EventsRepository>()

    route("/events") {
        get("/") {
            val command = ReadEventsCommand(eventRepository)
            command.execute()
            val found = command.getResult()

            if (found != null) {
                call.respond(HttpStatusCode.OK, found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        get("/{id}") {
            val command = ReadEventCommand(eventRepository, Id(call.parameters["id"] as String))
            command.execute()
            val found = command.getResult()

            if (found != null) {
                call.respond(HttpStatusCode.OK, found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        post("/") {
            val raw = JSONObject(call.receiveText())
            val command = CreateEventCommand(eventRepository, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result.id)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        put("/{id}") {
            val id = Id(call.parameters["id"] as String)
            val raw = JSONObject(call.receiveText())
            val command = UpdateEventCommand(eventRepository, id, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        delete("/{id}") {
            val command = DeleteEventCommand(eventRepository, Id(call.parameters["id"] as String))
            command.execute()
            val deleted = command.getResult()

            if (deleted) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    "Event still exist in database"
                )
            }
        }
    }

    route("events/{outgoingId}/events") {
        get("/{ingoingId}") {
            val outgoingId = Id(call.parameters["outgoingId"] as String)
            val ingoingId = Id(call.parameters["ingoingId"] as String)

            val command = CheckEventsCommand(eventRepository, outgoingId, ingoingId)
            command.execute()

            if (command.getResult() != null) {
                call.respond(
                    HttpStatusCode.OK, command.getResult().toString())
            } else {
                call.respond(HttpStatusCode.NoContent)
            }
        }
        put("/{ingoingId}") {
            val outgoingId = Id(call.parameters["outgoingId"] as String)
            val ingoingId = Id(call.parameters["ingoingId"] as String)
            val raw = JSONObject(call.receiveText())

            if (raw.has("label")) {
                val command = AttachEventsCommand(eventRepository, outgoingId, ingoingId, raw.getString("label"))
                command.execute()

                if (command.getResult()) {
                    call.respond(HttpStatusCode.Created)
                } else {
                    call.respond(HttpStatusCode.InternalServerError)
                }
            } else {
                call.respond(HttpStatusCode.BadRequest, "Missing label")
            }
        }
        delete("/{ingoingId}") {
            val outgoingId = Id(call.parameters["outgoingId"] as String)
            val ingoingId = Id(call.parameters["ingoingId"] as String)
            val raw = JSONObject(call.receiveText())

            if (raw.has("label")) {
                val command = AttachEventsCommand(eventRepository, outgoingId, ingoingId, raw.getString("label"))
                command.execute()

                if (command.getResult()) {
                    call.respond(HttpStatusCode.NoContent)
                } else {
                    call.respond(HttpStatusCode.InternalServerError)
                }
            } else {
                call.respond(HttpStatusCode.BadRequest, "Missing label")
            }
        }
    }
}