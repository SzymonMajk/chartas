package api

import commands.Status
import commands.connect.AttachCoordinatesCommand
import commands.connect.CheckCoordinatesCommand
import commands.connect.DetachCoordinatesCommand
import commands.create.*
import commands.delete.DeleteCoordinatesCommand
import commands.read.ReadCoordinateCommand
import commands.read.ReadCoordinatesCommand
import commands.update.UpdateCoordinatesCommand
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.*
import org.json.JSONObject
import org.koin.ktor.ext.inject
import repository.*
import repository.identification.Id

fun Routing.coordinates() {
    val eventRepository by inject<EventsRepository>()
    val coordinatesRepository by inject<CoordinatesRepository>()

    route("/coordinates") {
        get("/") {
            val command = ReadCoordinatesCommand(coordinatesRepository)
            command.execute()
            call.respond(HttpStatusCode.OK, command.getResult())
        }
        get("/{id}") {
            val command = ReadCoordinateCommand(coordinatesRepository, Id(call.parameters["id"] as String))
            command.execute()
            val found = command.getResult()

            if (found != null) {
                call.respond(HttpStatusCode.OK, found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        post("/") {
            val raw = JSONObject(call.receiveText())
            val command = CreateCoordinatesCommand(coordinatesRepository, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result.id)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        put("/{id}") {
            val id = Id(call.parameters["id"] as String)
            val raw = JSONObject(call.receiveText())
            val command = UpdateCoordinatesCommand(coordinatesRepository, id, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        delete("/{id}") {
            val command = DeleteCoordinatesCommand(
                coordinatesRepository,
                Id(call.parameters["id"] as String)
            )
            command.execute()
            val deleted = command.getResult()

            if (deleted) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    "Coordinates still exist in database"
                )
            }
        }
    }

    route("events/{eventId}/coordinates") {
        get("/{coordinatesId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val coordinatesId = Id(call.parameters["coordinatesId"] as String)
            val command = CheckCoordinatesCommand(eventRepository, coordinatesRepository, eventId, coordinatesId)

            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NoContent)
            }
        }
        put("/{coordinatesId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val coordinatesId = Id(call.parameters["coordinatesId"] as String)
            val command = AttachCoordinatesCommand(eventRepository, coordinatesRepository, eventId, coordinatesId)

            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.Created)
            } else {
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
        delete("/{coordinatesId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val coordinatesId = Id(call.parameters["coordinatesId"] as String)
            val command = DetachCoordinatesCommand(eventRepository, coordinatesRepository, eventId, coordinatesId)

            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
    }
}