package api

import commands.Status
import commands.connect.AttachReferenceCommand
import commands.connect.CheckReferenceCommand
import commands.connect.DetachReferenceCommand
import commands.create.CreateReferenceCommand
import commands.delete.DeleteReferenceCommand
import commands.read.ReadReferenceCommand
import commands.read.ReadReferencesCommand
import commands.update.UpdateReferenceCommand
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.*
import org.json.JSONObject
import org.koin.ktor.ext.inject
import repository.*
import repository.identification.Id

fun Routing.references() {
    val eventRepository by inject<EventsRepository>()
    val referenceRepository by inject<ReferencesRepository>()

    route("/references") {
        get("/") {
            val command = ReadReferencesCommand(referenceRepository)
            command.execute()
            call.respond(HttpStatusCode.OK, command.getResult())
        }
        get("/{id}") {
            val command = ReadReferenceCommand(
                referenceRepository,
                Id(call.parameters["id"] as String)
            )
            command.execute()
            val found = command.getResult()

            if (found != null) {
                call.respond(HttpStatusCode.OK, found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        post("/") {
            val raw = JSONObject(call.receiveText())

            val command = CreateReferenceCommand(
                referenceRepository,
                raw
            )
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result.id)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        put("/{id}") {
            val id = Id(call.parameters["id"] as String)
            val raw = JSONObject(call.receiveText())
            val command = UpdateReferenceCommand(referenceRepository, id, raw)
            command.execute()

            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        delete("/{id}") {
            val command = DeleteReferenceCommand(
                referenceRepository,
                Id(call.parameters["id"] as String)
            )
            command.execute()
            val deleted = command.getResult()

            if (deleted) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    "Reference still exist in database"
                )
            }
        }
    }

    route("events/{eventId}/references") {
        get("/{referenceId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val referenceId = Id(call.parameters["referenceId"] as String)
            val command = CheckReferenceCommand(eventRepository, referenceRepository, eventId, referenceId)
            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NoContent)
            }
        }
        put("/{referenceId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val referenceId = Id(call.parameters["referenceId"] as String)
            val command = AttachReferenceCommand(eventRepository, referenceRepository, eventId, referenceId)
            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.Created)
            } else {
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
        delete("/{referenceId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val referenceId = Id(call.parameters["referenceId"] as String)
            val command = DetachReferenceCommand(eventRepository, referenceRepository, eventId, referenceId)
            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(HttpStatusCode.InternalServerError)
            }
        }
    }
}