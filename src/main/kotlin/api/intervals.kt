package api

import commands.Status
import commands.connect.AttachIntervalCommand
import commands.connect.CheckIntervalCommand
import commands.connect.DetachIntervalCommand
import commands.create.CreateIntervalCommand
import commands.delete.DeleteIntervalCommand
import commands.read.ReadIntervalCommand
import commands.read.ReadIntervalsCommand
import commands.search.DrainOptimizerCommand
import commands.search.FillOptimizerCommand
import commands.update.UpdateIntervalCommand
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.*
import org.json.JSONObject
import org.koin.ktor.ext.inject
import repository.*
import repository.identification.Id
import search.SearchOptimizer

fun Routing.intervals() {
    val eventRepository by inject<EventsRepository>()
    val intervalRepository by inject<IntervalsRepository>()
    val optimizer by inject<SearchOptimizer>()

    route("/intervals") {
        get("/") {
            val command = ReadIntervalsCommand(intervalRepository)
            command.execute()

            call.respond(HttpStatusCode.OK, command.getResult())
        }
        get("/{id}") {
            val command = ReadIntervalCommand(intervalRepository, Id(call.parameters["id"] as String))
            command.execute()
            val found = command.getResult()

            if (found != null) {
                call.respond(HttpStatusCode.OK, found)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }
        post("/") {
            val raw = JSONObject(call.receiveText())
            val command = CreateIntervalCommand(intervalRepository, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result.id)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        put("/{id}") {
            val id = Id(call.parameters["id"] as String)
            val raw = JSONObject(call.receiveText())
            val command = UpdateIntervalCommand(intervalRepository, id, raw)
            command.execute()
            val result = command.getResult()

            when (result.status) {
                Status.UNLAUNCHED -> call.respond(HttpStatusCode.InternalServerError,
                    "Command result obtained before execution")
                Status.SUCCESS -> if (result.result != null) {
                    call.respond(HttpStatusCode.Created, result.result)
                } else {
                    call.respond(HttpStatusCode.InternalServerError,
                        "Actual result null despite success status")
                }
                Status.WRONG_INPUT -> call.respond(HttpStatusCode.BadRequest, "Wrong request parameters")
                Status.FAILURE -> call.respond(HttpStatusCode.InternalServerError,
                    "Wrong repository state after command exectution")
                Status.ERROR -> call.respond(HttpStatusCode.InternalServerError,
                    "Error occured during command execution")
            }
        }
        delete("/{id}") {
            val command = DeleteIntervalCommand(intervalRepository, Id(call.parameters["id"] as String))
            command.execute()
            val deleted = command.getResult()

            if (deleted) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    "Interval still exist in database"
                )
            }
        }
    }

    route("events/{eventId}/intervals") {
        get("/{intervalId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val intervalId = Id(call.parameters["intervalId"] as String)
            val command = CheckIntervalCommand(eventRepository, intervalRepository, eventId, intervalId)

            command.execute()

            if (command.getResult()) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NoContent)
            }
        }
        put("/{intervalId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val intervalId = Id(call.parameters["intervalId"] as String)
            val attachCommand = AttachIntervalCommand(eventRepository, intervalRepository, eventId, intervalId)
            val optimizerCommand = FillOptimizerCommand(eventRepository, optimizer, eventId)

            attachCommand.execute()

            if (attachCommand.getResult()) {
                optimizerCommand.execute()

                if (optimizerCommand.getResult()) {
                    call.respond(HttpStatusCode.Created)
                } else {
                    call.respond(HttpStatusCode.InternalServerError, "Problem occurs during optimizer update")
                }
            } else {
                call.respond(HttpStatusCode.InternalServerError, "Already attached or not existing in repository.")
            }
        }
        delete("/{intervalId}") {
            val eventId = Id(call.parameters["eventId"] as String)
            val intervalId = Id(call.parameters["intervalId"] as String)
            val detachCommand = DetachIntervalCommand(eventRepository, intervalRepository, eventId, intervalId)
            val optimizerCommand = DrainOptimizerCommand(eventRepository, optimizer, eventId)

            detachCommand.execute()

            if (detachCommand.getResult()) {
                optimizerCommand.execute()

                if (optimizerCommand.getResult()) {
                    call.respond(HttpStatusCode.Created)
                } else {
                    call.respond(HttpStatusCode.InternalServerError, "Problem occurs during optimizer update")
                }
            } else {
                call.respond(HttpStatusCode.InternalServerError, "Already detached or not existing in repository.")
            }
        }
    }
}