package domain

import export.ExportVisitor
import export.Exportable
import repository.identification.Id

class Relationship(
    val from : Id,
    val to : Id,
    val label : String
) : Exportable {

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeRelationship(this, state)
    }

        override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Relationship

        if (from != other.from) return false
        if (to != other.to) return false
        if (label != other.label) return false

        return true
    }

    override fun hashCode(): Int {
        return label.hashCode()
    }

    override fun toString(): String {
        return label
    }
}