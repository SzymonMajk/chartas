package domain

import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import export.Exportable
import repository.identification.Id

data class Reference(
    @JsonUnwrapped override val id : Id,
    val title : String,
    val author : String,
    val source : String
) : Identifiable, Exportable {

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeReference(this, state)
    }

        override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Reference

        if (id != other.id) return false
        if (title != other.title) return false
        if (author != other.author) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + author.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }

    override fun toString(): String {
        return id.toString()
    }
}