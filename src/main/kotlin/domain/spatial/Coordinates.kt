package domain.spatial

import domain.Identifiable
import export.Exportable

abstract class Coordinates : Identifiable, Exportable {

    override fun toString(): String {
        return id.toString()
    }
}