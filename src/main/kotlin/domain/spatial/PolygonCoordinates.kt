package domain.spatial

import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import repository.identification.Id

data class PolygonCoordinates(
    @JsonUnwrapped override val id : Id,
    var polygons: Array<Array<Array<Double>>>
) : Coordinates() {

    init {
        for (i in polygons.indices) {
            polygons[i] = refinePolygon(polygons[i])
        }
    }

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeCoordinates(this, state)
    }

    private fun refinePolygon(polygon : Array<Array<Double>>) =
        if (polygon.isNotEmpty() && !polygon[0].contentEquals(polygon[polygon.size - 1])) {
             var refined = emptyArray<Array<Double>>()
             refined = refined.plus(polygon)
             refined = refined.plus(polygon[0])
             refined
        } else {
            polygon
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PolygonCoordinates

        if (id != other.id) return false
        if (!polygons.contentDeepEquals(other.polygons)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = 31 + id.hashCode()
        result = 31 * result + polygons.contentDeepHashCode()
        return result
    }
}