package domain.spatial

import java.lang.ClassCastException

class ArrayTransformer {
    fun transformPoint(raw : Any) : Array<Double>? {
        val parsed = raw as? ArrayList<Double>

        return if (parsed != null && parsed.size == 2) {
            try {
                arrayOf(parsed[0], parsed[1])
            } catch (e : ClassCastException) {
                return null
            }
        } else {
            null
        }
    }

    fun transformLine(raw : Any) : Array<Array<Double>>? {
        val parsed = raw as? ArrayList<ArrayList<Double>>

        return if (parsed != null && parsed.isNotEmpty()) {
            try {
                parsed
                    .map { arrayOf(it[0], it[1]) }
                    .toTypedArray()
            } catch (e : ClassCastException) {
                return null
            }
        } else {
            null
        }
    }

    fun transformPolygons(raw : Any) : Array<Array<Array<Double>>>? {
        val parsed = raw as? ArrayList<ArrayList<ArrayList<Double>>>

        return if (parsed != null && parsed.isNotEmpty()) {
            try {
                parsed
                    .map { polygon ->
                        polygon
                            .map { arrayOf(it[0], it[1]) }
                            .toTypedArray()
                    }
                    .toTypedArray()
            } catch (e : ClassCastException) {
                return null
            }
        } else {
            null
        }
    }
}