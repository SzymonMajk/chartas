package domain.spatial


import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import repository.identification.Id

data class LineCoordinates(
    @JsonUnwrapped override val id : Id,
    val points: Array<Array<Double>>
) : Coordinates() {

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeCoordinates(this, state)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LineCoordinates

        if (id != other.id) return false
        if (!points.contentDeepEquals(other.points)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = 31 + id.hashCode()
        result = 31 * result + points.contentDeepHashCode()
        return result
    }
}