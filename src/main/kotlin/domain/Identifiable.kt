package domain

import repository.identification.Id


interface Identifiable {
    val id: Id
}