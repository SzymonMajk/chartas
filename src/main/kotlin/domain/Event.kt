package domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import domain.spatial.Coordinates
import domain.temporal.Interval
import export.Exportable
import repository.identification.Id

data class Event(@JsonUnwrapped override val id: Id,
                 val name: String,
                 val description: String,
                 @JsonIgnore val interval: Interval? = null,
                 @JsonIgnore val coordinates: Coordinates? = null,
                 @JsonIgnore var outgoingRelationships: Collection<Relationship> = emptyList(),
                 @JsonIgnore var references: Collection<Reference> = emptyList()
) : Identifiable, Exportable {

    override fun accept(visitor : ExportVisitor, state : String) : String {
        var proceedState = visitor.serializeEvent(this, state)
        if (interval != null) {
            proceedState = interval.accept(visitor, proceedState)
        }
        if (coordinates != null) {
            proceedState = coordinates.accept(visitor, proceedState)
        }
        references.forEach { proceedState = it.accept(visitor, proceedState) }
        outgoingRelationships.forEach { proceedState = it.accept(visitor, proceedState) }
        return proceedState
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Event

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + (interval?.hashCode() ?: 0)
        result = 31 * result + (coordinates?.hashCode() ?: 0)
        result = 31 * result + outgoingRelationships.hashCode()
        result = 31 * result + references.hashCode()
        return result
    }

    override fun toString(): String {
        return id.toString()
    }
}