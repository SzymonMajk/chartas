package domain.temporal


import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import repository.identification.Id
import java.time.LocalDate

data class FuzzyInterval (
    @JsonUnwrapped override val id: Id,
    override val date: LocalDate,
    override val duration : Int,
    val commenceUncertainty: Int,
    val terminationUncertainty: Int
) : Interval(id, date, duration) {

    override fun getLeftBorder(threshold: Double) : LocalDate {
        return date.minusDays((1 + commenceUncertainty * (1 - threshold)).toLong())
    }

    override fun getRightBorder(threshold: Double) : LocalDate {
        return date.plusDays((duration + 1 + terminationUncertainty * (1 - threshold)).toLong())
    }

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeInterval(this, state)
    }
}