package domain.temporal


import domain.Identifiable
import export.Exportable
import repository.identification.Id
import java.time.LocalDate

abstract class Interval(
    override val id: Id,
    open val date: LocalDate,
    open val duration : Int
): Identifiable, Exportable {

    abstract fun getLeftBorder(threshold : Double = 0.0) : LocalDate

    abstract fun getRightBorder(threshold : Double = 0.0) : LocalDate

    override fun toString(): String {
        return id.toString()
    }
}