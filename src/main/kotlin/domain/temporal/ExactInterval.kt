package domain.temporal


import com.fasterxml.jackson.annotation.JsonUnwrapped
import export.ExportVisitor
import repository.identification.Id
import java.time.LocalDate

data class ExactInterval(
    @JsonUnwrapped override val id: Id,
    override val date: LocalDate,
    override val duration : Int
) : Interval(id, date, duration) {

    override fun getLeftBorder(threshold: Double) : LocalDate {
        return date.minusDays(1)
    }

    override fun getRightBorder(threshold: Double) : LocalDate {
        return date.plusDays((duration + 1).toLong())
    }

    override fun accept(visitor : ExportVisitor, state : String) : String {
        return visitor.serializeInterval(this, state)
    }
}