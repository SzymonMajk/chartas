package repository

import domain.spatial.Coordinates
import repository.identification.Id
import repository.identification.IdGenerator

interface CoordinatesRepository {
    val generator : IdGenerator

    fun nextIdentity(): Id

    fun getAll() : Collection<Coordinates>

    fun get(id: Id) : Coordinates?

    fun insert(c: Coordinates) : Id?

    fun replace(c: Coordinates) : Coordinates?

    fun remove(id: Id) : Boolean
}