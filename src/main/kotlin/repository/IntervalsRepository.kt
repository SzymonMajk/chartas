package repository

import domain.temporal.Interval
import repository.identification.Id
import repository.identification.IdGenerator


interface IntervalsRepository {
    val generator : IdGenerator

    fun nextIdentity(): Id

    fun getAll() : Collection<Interval>

    fun get(id: Id) : Interval?

    fun insert(i: Interval) : Id?

    fun replace(i: Interval) : Interval?

    fun remove(id: Id) : Boolean
}