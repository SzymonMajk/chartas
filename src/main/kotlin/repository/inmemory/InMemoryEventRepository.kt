package repository.inmemory

import domain.Event
import repository.*
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator

open class InMemoryEventRepository(override val generator: IdGenerator) : EventsRepository {
    private var items = emptyMap<Id, Event>().toMutableMap()

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.EVENT)
    }

    override fun getAll(): Collection<Event> {
        return items.values
    }

    override fun get(id: Id): Event? {
        return items[id]
    }

    override fun insert(e: Event): Id? {
        if (!items.containsKey(e.id)) {
            items[e.id] = e
            return e.id
        }

        return null
    }

    override fun replace(e: Event): Event? {
        return items.replace(e.id, e)
    }

    override fun remove(id: Id): Boolean {
        items.remove(id)
        return !items.containsKey(id)
    }
}