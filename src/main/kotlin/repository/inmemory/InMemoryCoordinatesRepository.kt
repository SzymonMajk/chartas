package repository.inmemory

import domain.spatial.Coordinates
import repository.*
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator

open class InMemoryCoordinatesRepository(override val generator: IdGenerator) : CoordinatesRepository {
    private var items = emptyMap<Id, Coordinates>().toMutableMap()

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.COORDINATES)
    }

    override fun getAll(): Collection<Coordinates> {
        return items.values
    }

    override fun get(id: Id): Coordinates? {
        return items[id]
    }

    override fun insert(c: Coordinates): Id? {
        if (!items.containsKey(c.id)) {
            items[c.id] = c
            return c.id
        }

        return null
    }

    override fun replace(c: Coordinates): Coordinates? {
        return items.replace(c.id, c)
    }

    override fun remove(id: Id): Boolean {
        items.remove(id)
        return !items.containsKey(id)
    }
}