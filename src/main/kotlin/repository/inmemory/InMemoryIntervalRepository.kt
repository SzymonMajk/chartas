package repository.inmemory

import domain.temporal.Interval
import repository.*
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator

open class InMemoryIntervalRepository(override val generator: IdGenerator) : IntervalsRepository {
    private var items = emptyMap<Id, Interval>().toMutableMap()

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.INTERVAL)
    }

    override fun getAll(): Collection<Interval> {
        return items.values
    }

    override fun get(id: Id): Interval? {
        return items[id]
    }

    override fun insert(i: Interval): Id? {
        if (!items.containsKey(i.id)) {
            items[i.id] = i
            return i.id
        }

        return null
    }

    override fun replace(i: Interval): Interval? {
        return items.replace(i.id, i)
    }

    override fun remove(id: Id): Boolean {
        items.remove(id)
        return !items.containsKey(id)
    }
}