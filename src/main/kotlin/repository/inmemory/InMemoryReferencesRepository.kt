package repository.inmemory

import domain.Reference
import repository.*
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator

open class InMemoryReferencesRepository(override val generator: IdGenerator) : ReferencesRepository {
    private var items = emptyMap<Id, Reference>().toMutableMap()

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.REFERENCE)
    }

    override fun getAll(): Collection<Reference> {
        return items.values
    }

    override fun get(id: Id): Reference? {
        return items[id]
    }

    override fun insert(r: Reference): Id? {
        if (!items.containsKey(r.id)) {
            items[r.id] = r
            return r.id
        }

        return null
    }

    override fun replace(r: Reference): Reference? {
        return items.replace(r.id, r)
    }

    override fun remove(id: Id): Boolean {
        items.remove(id)
        return !items.containsKey(id)
    }
}