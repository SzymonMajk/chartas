package repository.identification

import repository.identification.Id
import repository.identification.IdBase

interface IdGenerator {
    fun nextId(idBase: IdBase) : Id
}