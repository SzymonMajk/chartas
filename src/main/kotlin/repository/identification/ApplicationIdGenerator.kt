package repository.identification

import java.util.concurrent.atomic.AtomicLong

class ApplicationIdGenerator : IdGenerator {

    private val eventsCounter = AtomicLong(0)
    private val datestampsCounter = AtomicLong(0)
    private val coordsCounter = AtomicLong(0)
    private val linksCounter = AtomicLong(0)
    private val referencesCounter = AtomicLong(0)

    override fun nextId(idBase: IdBase) : Id =
        when(idBase) {
            IdBase.EVENT -> Id(
                IdBase.EVENT.toString(),
                eventsCounter.incrementAndGet()
            )
            IdBase.INTERVAL -> Id(
                IdBase.INTERVAL.toString(),
                datestampsCounter.incrementAndGet()
            )
            IdBase.COORDINATES -> Id(
                IdBase.COORDINATES.toString(),
                coordsCounter.incrementAndGet()
            )
            IdBase.REFERENCE -> Id(
                IdBase.REFERENCE.toString(),
                referencesCounter.incrementAndGet()
            )
        }
}