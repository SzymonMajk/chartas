package repository.identification

enum class IdBase {
    EVENT, INTERVAL, COORDINATES, REFERENCE
}