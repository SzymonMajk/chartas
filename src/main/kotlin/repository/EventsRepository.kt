package repository

import domain.Event
import repository.identification.Id
import repository.identification.IdGenerator


interface EventsRepository {
    val generator : IdGenerator

    fun nextIdentity(): Id

    fun getAll() : Collection<Event>

    fun get(id: Id) : Event?

    fun insert(e: Event) : Id?

    fun replace(e: Event) : Event?

    fun remove(id: Id) : Boolean
}