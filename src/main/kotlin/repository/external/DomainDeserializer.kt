package repository.external

import com.fasterxml.jackson.databind.ObjectMapper
import domain.Event
import domain.Reference
import domain.Relationship
import domain.spatial.*
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import domain.temporal.Interval
import org.neo4j.driver.Record
import org.neo4j.driver.Value
import repository.identification.Id
import java.time.LocalDate

class DomainDeserializer {

    private val transformer = ArrayTransformer()

    fun parseEvent(
        record: Record, interval : Interval?,
        coordinates : Coordinates?,
        relationships : Collection<Relationship>,
        references : Collection<Reference>
    ) : Event? {
        val entity = record.values()[0]

        return if (entity.get("system_id").isNull || entity.get("name").isNull
            || entity.get("description").isNull) {
            null
        } else {
            Event(
                Id(entity.get("system_id").asString()),
                entity.get("name").asString(),
                entity.get("description").asString(),
                interval,
                coordinates,
                relationships,
                references
            )
        }
    }

    fun parseRelationship(values: List<Value>) =
        if (values.size != 3 || values[0].isNull || values[1].isNull || values[2].isNull) {
            null
        } else {
            Relationship(
                Id(values[0].asString()),
                Id(values[2].asString()),
                values[1].asString()
            )
        }

    fun parseReference(entity : Value) =
        if (entity.get("system_id").isNull || entity.get("title").isNull
            || entity.get("author").isNull || entity.get("source").isNull) {
            null
        } else {
            Reference(
                Id(entity.get("system_id").asString()),
                entity.get("title").asString(),
                entity.get("author").asString(),
                entity.get("source").asString()
            )
        }

    fun parseInterval(entity : Value) =
        if (entity.get("system_id").isNull || entity.get("date").isNull || entity.get("duration").isNull) {
            null
        } else if (!entity.get("commence_uncertainty").isNull && !entity.get("termination_uncertainty").isNull) {
            FuzzyInterval(
                Id(entity.get("system_id").asString()),
                LocalDate.parse(entity.get("date").asString()),
                entity.get("duration").asInt(),
                entity.get("commence_uncertainty").asInt(),
                entity.get("termination_uncertainty").asInt()
            )
        } else {
            ExactInterval(
                Id(entity.get("system_id").asString()),
                LocalDate.parse(entity.get("date").asString()),
                entity.get("duration").asInt()
            )
        }

     fun parseCoordinates(entity : Value) : Coordinates? {
        return if (entity.get("system_id").isNull || entity.get("coords").isNull) {
            null
        } else {
            val mapper = ObjectMapper()
            val coords = mapper.readValue(entity.get("coords").asString(), Any::class.java)
            val point = transformer.transformPoint(coords)

            if (point != null) {
                return PointCoordinates(
                    Id(entity.get("system_id").asString()),
                    point
                )
            }

            val line = transformer.transformLine(coords)

            if (line != null) {
                return LineCoordinates(
                    Id(entity.get("system_id").asString()),
                    line
                )
            }

            val polygons = transformer.transformPolygons(coords)

            if (polygons != null) {
                return PolygonCoordinates(
                    Id(entity.get("system_id").asString()),
                    polygons
                )
            }

            null
        }
    }
}