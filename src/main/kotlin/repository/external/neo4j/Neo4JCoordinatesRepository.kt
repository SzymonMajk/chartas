package repository.external.neo4j

import domain.spatial.Coordinates
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import org.json.JSONArray
import org.neo4j.driver.AuthTokens
import org.neo4j.driver.Driver
import org.neo4j.driver.GraphDatabase
import org.neo4j.driver.Values.parameters
import repository.CoordinatesRepository
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator
import repository.external.DomainDeserializer


open class Neo4JCoordinatesRepository(
    override val generator: IdGenerator,
    uri : String,
    user : String,
    password : String
) : CoordinatesRepository, AutoCloseable {

    private var driver: Driver? = null
    private val deserializer = DomainDeserializer()

    init {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password))
    }

    @Throws(Exception::class)
    override fun close() {
        driver!!.close()
    }

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.COORDINATES)
    }

    override fun getAll(): Collection<Coordinates> {
        var results = emptyList<Coordinates>()
        driver!!.session().use { session ->
        val result = session.run (
            "MATCH (c:Coordinates)\n" +
                    "RETURN c"
        )

         while (result.hasNext()) {
             val toCheck = deserializer.parseCoordinates(result.next().values()[0])

             if (toCheck != null) {
                 results = results.plus(toCheck)
             }
        }
    }
        return results
    }

    override fun get(id: Id): Coordinates? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (c:Coordinates {system_id: \$systemId})\n" +
                        "RETURN c",
                parameters("systemId", id.id)
            )

            return if (result.hasNext()) {
                deserializer.parseCoordinates(result.next().values()[0])
            } else {
                null
            }
        }
    }

    override fun insert(c: Coordinates) =
        when (c) {
            is PointCoordinates -> insertPoint(c)
            is LineCoordinates -> insertLine(c)
            is PolygonCoordinates -> insertPolygon(c)
            else -> null
        }

    override fun replace(c: Coordinates) =
        when (c) {
            is PointCoordinates -> replacePoint(c)
            is LineCoordinates -> replaceLine(c)
            is PolygonCoordinates -> replacePolygon(c)
            else -> null
        }

    override fun remove(id: Id): Boolean {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH(c:Coordinates {system_id: \$systemId})\n" +
                        "WITH c, c.system_id AS deleted_id\n" +
                        "DETACH DELETE c\n" +
                        "RETURN deleted_id",
                parameters("systemId", id.id)
            )

            return result.hasNext() && result.next().get(0).asString() == id.id
        }
    }


    private fun insertPoint(c : PointCoordinates) : Id? {
        driver!!.session().use { session ->
            val result = session.run(
                "MERGE (c:Coordinates {system_id: \$systemId})\n" +
                        "ON CREATE SET c.coords = \$coords\n" +
                        "RETURN c.system_id",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.lonLat).toString()
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    private fun insertLine(c : LineCoordinates) : Id? {
        driver!!.session().use { session ->
            val result = session.run(
                "MERGE (c:Coordinates {system_id: \$systemId})\n" +
                        "ON CREATE SET c.coords = \$coords\n" +
                        "RETURN c.system_id",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.points).toString()
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    private fun insertPolygon(c : PolygonCoordinates) : Id? {
        driver!!.session().use { session ->
            val result = session.run(
                "MERGE (c:Coordinates {system_id: \$systemId})\n" +
                        "ON CREATE SET c.coords = \$coords\n" +
                        "RETURN c.system_id",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.polygons).toString()
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    private fun replacePoint(c : PointCoordinates) : Coordinates? {
        driver!!.session().use { session ->
            val result = session.run(
                "MATCH (c:Coordinates {system_id: \$systemId})\n" +
                        "SET c.coords = \$coords\n" +
                        "RETURN c",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.lonLat).toString()
                )
            )

            return if (result.hasNext()) {
                deserializer.parseCoordinates(result.next().values()[0])
            } else {
                null
            }
        }
    }

    private fun replaceLine(c : LineCoordinates) : Coordinates? {
        driver!!.session().use { session ->
            val result = session.run(
                "MATCH (c:Coordinates {system_id: \$systemId})\n" +
                        "SET c.coords = \$coords\n" +
                        "RETURN c",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.points).toString()
                )
            )

            return if (result.hasNext()) {
                deserializer.parseCoordinates(result.next().values()[0])
            } else {
                null
            }
        }
    }

    private fun replacePolygon(c : PolygonCoordinates) : Coordinates? {
        driver!!.session().use { session ->
            val result = session.run(
                "MATCH (c:Coordinates {system_id: \$systemId})\n" +
                        "SET c.coords = \$coords\n" +
                        "RETURN c",
                parameters(
                    "systemId", c.id.id,
                    "coords", JSONArray(c.polygons).toString()
                )
            )

            return if (result.hasNext()) {
                deserializer.parseCoordinates(result.next().values()[0])
            } else {
                null
            }
        }
    }
}