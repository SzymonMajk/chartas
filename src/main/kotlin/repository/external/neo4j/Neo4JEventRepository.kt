package repository.external.neo4j

import domain.Event
import domain.Reference
import domain.Relationship
import domain.spatial.Coordinates
import domain.temporal.Interval
import org.neo4j.driver.*
import org.neo4j.driver.Values.parameters
import repository.*
import repository.external.DomainDeserializer
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator

open class Neo4JEventRepository(
    override val generator: IdGenerator,
    uri : String,
    user : String,
    password : String
) : EventsRepository, AutoCloseable {

    private var driver: Driver? = null
    private val deserializer = DomainDeserializer()
    private val intervalLabel = "WHEN"
    private val coodsLabel = "WHERE"
    private val refLabel = "REFERENCED"

    init {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password))
    }

    @Throws(Exception::class)
    override fun close() {
        driver!!.close()
    }

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.EVENT)
    }

    override fun getAll(): Collection<Event> {
        var ids = emptyList<Id>()
        var results = emptyList<Event?>()
        driver!!.session().use { session ->
            val result = session.run(
                "MATCH (e:Event)\n" +
                        "RETURN e.system_id"
            )

            while (result.hasNext()) {
                ids = ids.plus(Id(result.next().values()[0].asString()))
            }
        }

        ids.forEach { results = results.plus(get(it)) }
        return results.filterNotNull()
    }

    override fun get(id: Id): Event? {
        driver!!.session().use { session ->
            val eventResult = session.run (
                "MATCH (a:Event {system_id: \$systemId})\n" +
                        "RETURN a",
                parameters("systemId", id.id)
            )

            val intervalResult = session.run (
                "MATCH (e:Event {system_id: \$systemId})-[:$intervalLabel]->(i:Interval)\n" +
                        "RETURN i",
                parameters("systemId", id.id)
            )

            val coordinatesResult = session.run (
                "MATCH (e:Event {system_id: \$systemId})-[:$coodsLabel]->(c:Coordinates)\n" +
                        "RETURN c",
                parameters("systemId", id.id)
            )

            val referencesResults = session.run(
                "MATCH (e:Event {system_id: \$systemId})-[:$refLabel]->(r:Reference)\n" +
                        "RETURN r",
                parameters("systemId", id.id)
            )

            val relationshipsResults = session.run(
                "MATCH (out:Event {system_id: \$outId})-[l]->(in:Event)\n" +
                        "RETURN out.system_id, TYPE(l), in.system_id",
                parameters("outId", id.id)
            )

            return if (eventResult.hasNext()) {
                var interval : Interval? = null
                var coordinates : Coordinates? = null
                var references : Collection<Reference> = emptyList()
                var relationships : Collection<Relationship> = emptyList()

                if (intervalResult.hasNext()) {
                    interval = deserializer.parseInterval(intervalResult.next().values()[0])
                }

                if (coordinatesResult.hasNext()) {
                    coordinates = deserializer.parseCoordinates(coordinatesResult.next().values()[0])
                }

                while (referencesResults.hasNext()) {
                    val reference = deserializer.parseReference(referencesResults.next().values()[0])
                    if (reference != null) {
                        references = references.plus(reference)
                    }
                }

                while (relationshipsResults.hasNext()) {
                    val relationship = deserializer.parseRelationship(relationshipsResults.next().values())
                    if (relationship != null) {
                        relationships = relationships.plus(relationship)
                    }
                }

                deserializer.parseEvent(eventResult.next(), interval, coordinates, relationships, references)
            } else {
                null
            }
        }
    }

    override fun insert(e: Event): Id? {
        var resultId : Id? = null

        driver!!.session().use { session ->
            val result = session.run (
                "MERGE (a:Event {system_id: \$systemId})\n" +
                        "ON CREATE SET a.name = \$name, a.description = \$description\n" +
                        "RETURN a.system_id",
                parameters(
                    "systemId", e.id.id,
                    "name", e.name,
                    "description", e.description
                )
            )

            if (result.hasNext()) {
                resultId = Id(result.next().get(0).asString())
            }
        }

        if (e.interval != null) {
            driver!!.session().use { session ->
                session.run(
                    "MATCH(e:Event {system_id: \$eventId})\n" +
                            "MATCH(i:Interval {system_id: \$intervalId})\n" +
                            "MERGE(e)-[:$intervalLabel]->(i)",
                    parameters(
                        "eventId", e.id.id,
                        "intervalId", e.interval.id.id
                    )
                )
            }
        }

        if (e.coordinates != null) {
            driver!!.session().use { session ->
                session.run(
                    "MATCH(e:Event {system_id: \$eventId})\n" +
                            "MATCH(c:Coordinates {system_id: \$coordinatesId})\n" +
                            "MERGE(e)-[:$coodsLabel]->(c)",
                    parameters(
                        "eventId", e.id.id,
                        "coordinatesId", e.coordinates.id.id
                    )
                )
            }
        }

        if (e.references.isNotEmpty()) {
            driver!!.session().use { session ->
                e.references.forEach {
                    session.run(
                        "MATCH(e:Event {system_id: \$eventId})\n" +
                                "MATCH(r:Reference {system_id: \$referenceId})\n" +
                                "MERGE(e)-[:$refLabel]->(r)",
                        parameters(
                            "eventId", e.id.id,
                            "referenceId", it.id.id
                        )
                    )
                }
            }
        }

        if (e.outgoingRelationships.isNotEmpty()) {
            driver!!.session().use { session ->
                e.outgoingRelationships.forEach {
                    val label = it.label
                    session.run(
                        "MATCH(out:Event {system_id: \$outId})\n" +
                                "MATCH(in:Event {system_id: \$inId})\n" +
                                "MERGE(out)-[: $label]->(in)",
                        parameters(
                            "outId", it.from.id,
                            "inId", it.to.id
                        )
                    )
                }
            }
        }

        return resultId
    }

    override fun replace(e: Event): Event? {
        val id = e.id
        remove(id)
        insert(e)
        return get(id)
    }

    override fun remove(id: Id): Boolean {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH(a:Event {system_id: \$systemId})\n" +
                        "WITH a, a.system_id AS deleted_id\n" +
                        "DETACH DELETE a\n" +
                        "RETURN deleted_id",
                parameters("systemId", id.id)
            )

            return result.hasNext() && result.next().get(0).asString() == id.id
        }
    }
}