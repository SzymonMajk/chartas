package repository.external.neo4j

import domain.Reference
import org.neo4j.driver.AuthTokens
import org.neo4j.driver.Driver
import org.neo4j.driver.GraphDatabase
import org.neo4j.driver.Values.parameters
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator
import repository.ReferencesRepository
import repository.external.DomainDeserializer


open class Neo4JReferenceRepository(
    override val generator: IdGenerator,
    uri : String,
    user : String,
    password : String
) : ReferencesRepository, AutoCloseable {

    private var driver: Driver? = null
    private val deserializer = DomainDeserializer()

    init {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password))
    }

    @Throws(Exception::class)
    override fun close() {
        driver!!.close()
    }

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.REFERENCE)
    }

    override fun getAll(): Collection<Reference> {
        var results = emptyList<Reference>()
        driver!!.session().use { session ->
        val result = session.run (
            "MATCH (a:Reference)\n" +
                    "RETURN a"
        )

         while (result.hasNext()) {
             val toCheck = deserializer.parseReference(result.next().values()[0])

             if (toCheck != null) {
                 results = results.plus(toCheck)
             }
        }
    }
        return results
    }

    override fun get(id: Id): Reference? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (r:Reference {system_id: \$systemId})\n" +
                        "RETURN r",
                parameters("systemId", id.id)
            )

            return if (result.hasNext()) {
                deserializer.parseReference(result.next().values()[0])
            } else {
                null
            }
        }
    }

    override fun insert(r: Reference): Id? {
        driver!!.session().use { session ->
            val result = session.run (
                "MERGE (r:Reference {system_id: \$systemId})\n" +
                        "ON CREATE SET r.title = \$title, r.author = \$author, r.source = \$source\n" +
                        "RETURN r.system_id",
                parameters(
                    "systemId", r.id.id,
                    "title", r.title,
                    "author", r.author,
                    "source", r.source
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    override fun replace(r: Reference): Reference? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (r:Reference {system_id: \$systemId})\n" +
                        "SET r.title = \$title, r.author = \$author, r.source = \$source\n" +
                        "RETURN r",
                parameters(
                    "systemId", r.id.id,
                    "title", r.title,
                    "author", r.author,
                    "source", r.source
                )
            )

            return if (result.hasNext()) {
                deserializer.parseReference(result.next().values()[0])
            } else {
                null
            }
        }
    }

    override fun remove(id: Id): Boolean {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH(r:Reference {system_id: \$systemId})\n" +
                        "WITH r, r.system_id AS deleted_id\n" +
                        "DETACH DELETE r\n" +
                        "RETURN deleted_id",
                parameters("systemId", id.id)
            )

            return result.hasNext() && result.next().get(0).asString() == id.id
        }
    }
}