package repository.external.neo4j

import domain.temporal.FuzzyInterval
import domain.temporal.Interval
import org.neo4j.driver.AuthTokens
import org.neo4j.driver.Driver
import org.neo4j.driver.GraphDatabase
import org.neo4j.driver.Values.parameters
import repository.identification.Id
import repository.identification.IdBase
import repository.identification.IdGenerator
import repository.IntervalsRepository
import repository.external.DomainDeserializer


open class Neo4JIntervalRepository(
    override val generator: IdGenerator,
    uri : String,
    user : String,
    password : String
) : IntervalsRepository, AutoCloseable {

    private var driver: Driver? = null
    private val deserializer = DomainDeserializer()

    init {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password))
    }

    @Throws(Exception::class)
    override fun close() {
        driver!!.close()
    }

    override fun nextIdentity(): Id {
        return generator.nextId(IdBase.INTERVAL)
    }

    override fun getAll(): Collection<Interval> {
        var results = emptyList<Interval>()
        driver!!.session().use { session ->
        val result = session.run (
            "MATCH (i:Interval)\n" +
                    "RETURN i"
        )

         while (result.hasNext()) {
             val toCheck = deserializer.parseInterval(result.next().values()[0])

             if (toCheck != null) {
                 results = results.plus(toCheck)
             }
        }
    }
        return results
    }

    override fun get(id: Id): Interval? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (i:Interval {system_id: \$systemId})\n" +
                        "RETURN i",
                parameters("systemId", id.id)
            )

            return if (result.hasNext()) {
                deserializer.parseInterval(result.next().values()[0])
            } else {
                null
            }
        }
    }

    override fun insert(i: Interval): Id? {
        return if (i is FuzzyInterval) {
            insertFuzzy(i)
        } else {
            insertExact(i)
        }
    }

    override fun replace(i: Interval): Interval? {
        return if (i is FuzzyInterval) {
            replaceFuzzy(i)
        } else {
            replaceExact(i)
        }
    }

    override fun remove(id: Id): Boolean {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH(i:Interval {system_id: \$systemId})\n" +
                        "WITH i, i.system_id AS deleted_id\n" +
                        "DETACH DELETE i\n" +
                        "RETURN deleted_id",
                parameters("systemId", id.id)
            )

            return result.hasNext() && result.next().get(0).asString() == id.id
        }
    }

    private fun insertExact(i : Interval) : Id? {
        driver!!.session().use { session ->
            val result = session.run (
                "MERGE (i:Interval {system_id: \$systemId})\n" +
                        "ON CREATE SET i.date = \$date, i.duration = \$duration\n" +
                        "RETURN i.system_id",
                parameters(
                    "systemId", i.id.id,
                    "date", i.date.toString(),
                    "duration", i.duration
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    private fun insertFuzzy(i : FuzzyInterval) : Id? {
        driver!!.session().use { session ->
            val result = session.run (
                "MERGE (i:Interval {system_id: \$systemId})\n" +
                        "ON CREATE SET i.date = \$date, i.duration = \$duration," +
                            "i.commence_uncertainty = \$commence, i.termination_uncertainty = \$termination\n" +
                        "RETURN i.system_id",
                parameters(
                    "systemId", i.id.id,
                    "date", i.date.toString(),
                    "duration", i.duration,
                    "commence", i.commenceUncertainty,
                    "termination", i.terminationUncertainty
                )
            )

            return if (result.hasNext()) {
                Id(result.next().get(0).asString())
            } else {
                null
            }
        }
    }

    private fun replaceExact(i : Interval) : Interval? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (i:Interval {system_id: \$systemId})\n" +
                        "SET i.date = \$date, i.duration = \$duration\n" +
                        "RETURN i",
                parameters(
                    "systemId", i.id.id,
                    "date", i.date.toString(),
                    "duration", i.duration
                )
            )

            return if (result.hasNext()) {
                deserializer.parseInterval(result.next().values()[0])
            } else {
                null
            }
        }
    }

    private fun replaceFuzzy(i : FuzzyInterval) : Interval? {
        driver!!.session().use { session ->
            val result = session.run (
                "MATCH (i:Interval {system_id: \$systemId})\n" +
                        "SET i.date = \$date, i.duration = \$duration," +
                        "i.commence_uncertainty = \$commence, i.termination_uncertainty = \$termination\n" +
                        "RETURN i",
                parameters(
                    "systemId", i.id.id,
                    "date", i.date.toString(),
                    "duration", i.duration,
                    "commence", i.commenceUncertainty,
                    "termination", i.terminationUncertainty
                )
            )

            return if (result.hasNext()) {
                deserializer.parseInterval(result.next().values()[0])
            } else {
                null
            }
        }
    }
}