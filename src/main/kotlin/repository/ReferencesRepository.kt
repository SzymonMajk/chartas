package repository

import domain.Reference
import repository.identification.Id
import repository.identification.IdGenerator


interface ReferencesRepository {
    val generator : IdGenerator

    fun nextIdentity(): Id

    fun getAll() : Collection<Reference>

    fun get(id: Id) : Reference?

    fun insert(r: Reference) : Id?

    fun replace(r: Reference) : Reference?

    fun remove(id: Id) : Boolean
}