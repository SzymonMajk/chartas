package commands.delete

import commands.Command
import repository.EventsRepository
import repository.identification.Id

class DeleteEventCommand(
    private val receiver: EventsRepository,
    private val id: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.remove(id)
    }
}