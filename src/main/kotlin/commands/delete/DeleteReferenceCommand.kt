package commands.delete

import commands.Command
import repository.identification.Id
import repository.ReferencesRepository

class DeleteReferenceCommand(
    private val receiver: ReferencesRepository,
    private val id: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.remove(id)
    }
}