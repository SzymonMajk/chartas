package commands.delete

import commands.Command
import repository.CoordinatesRepository
import repository.identification.Id

class DeleteCoordinatesCommand(
    private val receiver: CoordinatesRepository,
    private val id: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.remove(id)
    }
}