package commands.delete

import commands.Command
import repository.identification.Id
import repository.IntervalsRepository

class DeleteIntervalCommand(
    private val receiver: IntervalsRepository,
    private val id: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.remove(id)
    }
}