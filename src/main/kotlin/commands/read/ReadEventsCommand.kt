package commands.read

import commands.Command
import export.DomainJsonExporter
import repository.EventsRepository

class ReadEventsCommand(
    private val receiver: EventsRepository
) : Command<String?> {

    private var innerResult : String? = null
    private val initialState = "{}"

    override fun getResult() : String? {
        return innerResult
    }

    override fun execute() {
        val events = receiver.getAll()
        var state = initialState
        val exporter = DomainJsonExporter()

        events.forEach { state = it.accept(exporter, state) }

        innerResult = state
    }
}