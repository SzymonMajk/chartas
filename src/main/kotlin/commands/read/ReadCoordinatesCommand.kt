package commands.read

import commands.Command
import domain.spatial.Coordinates
import repository.CoordinatesRepository

class ReadCoordinatesCommand(
    private val receiver: CoordinatesRepository
) : Command<Collection<Coordinates>> {

    private var innerResult : Collection<Coordinates> = emptyList()

    override fun getResult() : Collection<Coordinates> {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.getAll()
    }
}