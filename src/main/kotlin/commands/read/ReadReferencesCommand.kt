package commands.read

import commands.Command
import domain.Reference
import repository.ReferencesRepository

class ReadReferencesCommand(
    private val receiver: ReferencesRepository
) : Command<Collection<Reference>> {

    private var innerResult : Collection<Reference> = emptyList()

    override fun getResult() : Collection<Reference> {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.getAll()
    }
}