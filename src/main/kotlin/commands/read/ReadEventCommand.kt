package commands.read

import commands.Command
import export.DomainJsonExporter
import repository.EventsRepository
import repository.identification.Id

class ReadEventCommand(
    private val receiver: EventsRepository,
    private val id: Id
) : Command<String?> {

    private var innerResult : String? = null
    private val initialState = "{}"

    override fun getResult() : String? {
        return innerResult
    }

    override fun execute() {
        val event = receiver.get(id)

        if (event != null) {
            innerResult = event.accept(DomainJsonExporter(), initialState)
        }
    }
}