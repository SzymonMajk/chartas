package commands.read

import commands.Command
import domain.temporal.Interval
import repository.IntervalsRepository

class ReadIntervalsCommand(
    private val receiver: IntervalsRepository
) : Command<Collection<Interval>> {

    private var innerResult : Collection<Interval> = emptyList()

    override fun getResult() : Collection<Interval> {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.getAll()
    }
}