package commands.read

import commands.Command
import domain.Reference
import repository.identification.Id
import repository.ReferencesRepository

class ReadReferenceCommand(
    private val receiver: ReferencesRepository,
    private val id: Id
) : Command<Reference> {

    private var innerResult : Reference? = null

    override fun getResult() : Reference? {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.get(id)
    }
}