package commands.read

import commands.Command
import domain.temporal.Interval
import repository.identification.Id
import repository.IntervalsRepository

class ReadIntervalCommand(
    private val receiver: IntervalsRepository,
    private val id: Id
) : Command<Interval> {

    private var innerResult : Interval? = null

    override fun getResult() : Interval? {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.get(id)
    }
}