package commands.read

import commands.Command
import domain.spatial.Coordinates
import repository.CoordinatesRepository
import repository.identification.Id

class ReadCoordinateCommand(
    private val receiver: CoordinatesRepository,
    private val id: Id
) : Command<Coordinates> {

    private var innerResult : Coordinates? = null

    override fun getResult() : Coordinates? {
        return innerResult
    }

    override fun execute() {
        innerResult = receiver.get(id)
    }
}