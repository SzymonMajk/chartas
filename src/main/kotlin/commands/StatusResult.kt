package commands

data class StatusResult<T>(
    val status : Status,
    val result : T
)