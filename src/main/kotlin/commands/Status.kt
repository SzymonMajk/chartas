package commands

enum class Status {
    UNLAUNCHED, WRONG_INPUT, SUCCESS, FAILURE, ERROR
}