package commands.search

import commands.Command
import domain.Event
import search.SearchOptimizer
import java.time.LocalDate

class SearchCommand(
    private val optimizer: SearchOptimizer,
    private val start: LocalDate,
    private val end: LocalDate,
    private val rawThreshold: String?
) : Command<Collection<Event>> {

    private var innerResult : Collection<Event> = emptyList()

    override fun getResult() : Collection<Event> {
        return innerResult
    }

    override fun execute() {
        if (rawThreshold != null) {
            val threshold = rawThreshold.toDoubleOrNull()

            if (threshold != null)
                innerResult = when {
                    threshold >= 1.0 -> {
                        optimizer.getVisible(start, end, 1.0)
                    }
                    threshold <= 0.0 -> {
                        optimizer.getVisible(start, end, 0.0)
                    }
                    else -> {
                        optimizer.getVisible(start, end, threshold)
                    }
                }
        }
        innerResult = optimizer.getVisible(start, end)
    }
}