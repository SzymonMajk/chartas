package commands.search

import commands.Command
import repository.EventsRepository
import repository.identification.Id
import search.SearchOptimizer

class DrainOptimizerCommand (
    private val eventsReceiver : EventsRepository,
    private val optimizer: SearchOptimizer,
    private val eventId: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        val event = eventsReceiver.get(eventId)

        if (event != null) {
            optimizer.remove(event)
            innerResult = true
        }
    }
}