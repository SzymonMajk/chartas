package commands.search

import commands.Command
import domain.Event
import export.ExportVisitor
import export.GeoJsonExporter
import export.GraphJsonExporter
import export.TimelineJsonExporter

class ExportCommand(
    private val format: String?,
    private val events: Collection<Event>
) : Command<String?> {

    private var innerResult : String? = null
    private var initialState : String? = null

    override fun getResult() : String? {
        return innerResult
    }

    override fun execute() {
        val exporter = selectExporter(format)

        if (exporter != null && initialState != null) {
            var state = initialState as String
            events.forEach { state = it.accept(exporter, state) }
            innerResult = state
        }
    }

    private fun selectExporter(format: String?) : ExportVisitor? {
        if (format == null) {
            initialState = "{}"
            return TimelineJsonExporter()
        } else {
            when (format) {
                "chronology" -> {
                    initialState = "{}"
                    return TimelineJsonExporter()
                }
                "geojson" -> {
                    initialState = "{}"
                    return GeoJsonExporter()
                }
                "graph" -> {
                    initialState = "{}"
                    return GraphJsonExporter()
                }
            }
        }

        return null
    }
}