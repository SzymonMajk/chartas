package commands.create

import com.fasterxml.jackson.databind.ObjectMapper
import commands.Command
import commands.StatusResult
import commands.Status
import domain.spatial.ArrayTransformer
import domain.spatial.LineCoordinates
import domain.spatial.PointCoordinates
import domain.spatial.PolygonCoordinates
import org.json.JSONObject
import repository.CoordinatesRepository
import repository.identification.Id

class CreateCoordinatesCommand(
    private val receiver: CoordinatesRepository,
    private val raw: JSONObject
) : Command<StatusResult<Id?>> {

    private var innerResult : StatusResult<Id?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Id?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)

        if (raw.has("coordinates")) {
            val transformer = ArrayTransformer()
            val mapper = ObjectMapper()
            val coords = mapper.readValue(raw.get("coordinates").toString(), Any::class.java)
            val point = transformer.transformPoint(coords)

            if (point != null) {
                val id = receiver.insert(
                    PointCoordinates(
                        receiver.nextIdentity(),
                        point
                    )
                )

                innerResult = if (id != null) {
                    StatusResult(Status.SUCCESS, id)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }

            val line = transformer.transformLine(coords)

            if (line != null) {
                val id = receiver.insert(
                    LineCoordinates(
                        receiver.nextIdentity(),
                        line
                    )
                )

                innerResult = if (id != null) {
                    StatusResult(Status.SUCCESS, id)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }

            val polygons = transformer.transformPolygons(coords)

            if (polygons != null) {
                val id = receiver.insert(
                    PolygonCoordinates(
                        receiver.nextIdentity(),
                        polygons
                    )
                )

                innerResult = if (id != null) {
                    StatusResult(Status.SUCCESS, id)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }
        }

        innerResult = StatusResult(Status.WRONG_INPUT, null)
    }
}