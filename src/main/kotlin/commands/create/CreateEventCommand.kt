package commands.create

import commands.Command
import commands.StatusResult
import commands.Status
import domain.Event
import org.json.JSONObject
import repository.EventsRepository
import repository.identification.Id

class CreateEventCommand(
    private val receiver: EventsRepository,
    private val raw: JSONObject
) : Command<StatusResult<Id?>> {

    private var innerResult : StatusResult<Id?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Id?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)

        if (raw.has("name") && raw.has("description")) {
            val id= receiver.insert(
                Event(
                    receiver.nextIdentity(),
                    raw.getString("name"),
                    raw.getString("description")
                )
            )

            innerResult = if (id != null) {
                StatusResult(Status.SUCCESS, id)
            } else {
                StatusResult(Status.FAILURE, null)
            }
        } else {
            innerResult = StatusResult(Status.WRONG_INPUT, null)
        }
    }
}