package commands.create

import commands.Command
import commands.StatusResult
import commands.Status
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import org.json.JSONObject
import repository.identification.Id
import repository.IntervalsRepository
import java.time.LocalDate

class CreateIntervalCommand(
    private val receiver: IntervalsRepository,
    private val raw: JSONObject
) : Command<StatusResult<Id?>> {

    private var innerResult : StatusResult<Id?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Id?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)

        if (raw.has("date") && raw.has("duration")) {
            val date = LocalDate.parse(raw.getString("date"))

            if (checkDate(date) && checkInt("duration", raw)) {
                if (raw.has("commenceUncertainty") && raw.has("terminationUncertainty")) {
                    if (checkInt("commenceUncertainty", raw) && checkInt("terminationUncertainty", raw)) {
                        val id = receiver.insert(
                            FuzzyInterval(
                                receiver.nextIdentity(),
                                date,
                                raw.getInt("duration"),
                                raw.getInt("commenceUncertainty"),
                                raw.getInt("terminationUncertainty")
                            )
                        )

                        innerResult = if (id != null) {
                            StatusResult(Status.SUCCESS, id)
                        } else {
                            StatusResult(Status.FAILURE, null)
                        }
                    } else {
                        innerResult = StatusResult(Status.WRONG_INPUT, null)
                    }
                } else {
                    val id = receiver.insert(
                        ExactInterval(
                            receiver.nextIdentity(),
                            date,
                            raw.getInt("duration")
                        )
                    )

                    innerResult = if (id != null) {
                        StatusResult(Status.SUCCESS, id)
                    } else {
                        StatusResult(Status.FAILURE, null)
                    }
                }
            } else {
                innerResult = StatusResult(Status.WRONG_INPUT, null)
            }
        }
    }

    private fun checkDate(date : LocalDate?) : Boolean {
        return date != null
    }

    private fun checkInt(name : String, raw : JSONObject) : Boolean {
        return try {
            raw.getInt(name)
            true
        } catch (e : ClassCastException) {
            false
        }
    }
}