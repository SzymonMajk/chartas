package commands.create

import commands.Command
import commands.StatusResult
import commands.Status
import domain.Reference
import org.json.JSONObject
import repository.identification.Id
import repository.ReferencesRepository

class CreateReferenceCommand(
    private val receiver: ReferencesRepository,
    private val raw: JSONObject
) : Command<StatusResult<Id?>> {

    private var innerResult : StatusResult<Id?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Id?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)

        if (raw.has("title") && raw.has("author") && raw.has("source")) {
            val id = receiver.insert(
                Reference(
                    receiver.nextIdentity(),
                    raw.getString("title"),
                    raw.getString("author"),
                    raw.getString("source")
                )
            )

            innerResult = if (id != null) {
                StatusResult(Status.SUCCESS, id)
            } else {
                StatusResult(Status.FAILURE, null)
            }
        } else {
            innerResult = StatusResult(Status.WRONG_INPUT, null)
        }
    }
}