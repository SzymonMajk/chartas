package commands.connect

import commands.Command
import repository.EventsRepository
import repository.IntervalsRepository
import repository.identification.Id

class CheckIntervalCommand(
    private val eventsReceiver : EventsRepository,
    private val intervalsReceiver : IntervalsRepository,
    private val eventId: Id,
    private val intervalId: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        val event = eventsReceiver.get(eventId)
        val interval = intervalsReceiver.get(intervalId)

        if (event != null && interval != null && event.interval != null && event.interval.id == intervalId) {
            innerResult = true
        }
    }
}