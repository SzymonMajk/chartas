package commands.connect

import commands.Command
import repository.EventsRepository
import repository.identification.Id

class CheckEventsCommand(
    private val receiver : EventsRepository,
    private val outgoingId: Id,
    private val ingoingId: Id
) : Command<String?> {

    private var innerResult : String? = null

    override fun getResult() : String? {
        return innerResult
    }

    override fun execute() {
        val first = receiver.get(outgoingId)
        val second = receiver.get(ingoingId)

        if (first != null && second != null && first.outgoingRelationships.any { e -> e.to == ingoingId }) {
            innerResult = first.outgoingRelationships.find { e -> e.to == ingoingId }!!.label
        }
    }
}
