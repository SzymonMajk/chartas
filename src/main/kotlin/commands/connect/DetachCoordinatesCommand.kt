package commands.connect

import commands.Command
import domain.Event
import repository.CoordinatesRepository
import repository.EventsRepository
import repository.identification.Id

class DetachCoordinatesCommand(
    private val eventsReceiver : EventsRepository,
    private val coordinatedReceiver : CoordinatesRepository,
    private val eventId: Id,
    private val coordinatesId: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        val event = eventsReceiver.get(eventId)
        val coords = coordinatedReceiver.get(coordinatesId)

        if (event != null && coords != null && event.coordinates != null && event.coordinates.id == coordinatesId) {
            val updatedEvent = Event(
                event.id,
                event.name,
                event.description,
                event.interval,
                null,
                event.outgoingRelationships,
                event.references
            )

            eventsReceiver.replace(updatedEvent)
            innerResult = true
        }
    }
}
