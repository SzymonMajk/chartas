package commands.connect

import commands.Command
import domain.Relationship
import repository.EventsRepository
import repository.identification.Id

class DetachEventsCommand(
    private val receiver : EventsRepository,
    private val outgoingId: Id,
    private val ingoingId: Id,
    private val label: String
) : Command<Boolean> {

    private var innerResult: Boolean = false

    override fun getResult(): Boolean {
        return innerResult
    }

    override fun execute() {
        val first = receiver.get(outgoingId)
        val second = receiver.get(ingoingId)

        if (first != null && second != null && first.outgoingRelationships.any { e -> e.to == ingoingId }) {
            val relationship = Relationship(first.id, second.id, label)
            first.outgoingRelationships = first.outgoingRelationships.minus(relationship)
            receiver.replace(first)
            innerResult = true
        }
    }
}