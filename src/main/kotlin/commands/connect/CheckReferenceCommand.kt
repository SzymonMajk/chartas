package commands.connect

import commands.Command
import repository.EventsRepository
import repository.ReferencesRepository
import repository.identification.Id

class CheckReferenceCommand(
    private val eventsReceiver : EventsRepository,
    private val referenceReceiver : ReferencesRepository,
    private val eventId: Id,
    private val referenceId: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        val event = eventsReceiver.get(eventId)
        val reference = referenceReceiver.get(referenceId)

        if (event != null && reference != null && event.references.any { r -> r.id == referenceId}) {
            innerResult = true
        }
    }
}
