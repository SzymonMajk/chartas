package commands.connect

import commands.Command
import domain.Event
import repository.EventsRepository
import repository.IntervalsRepository
import repository.identification.Id

class AttachIntervalCommand(
    private val eventsReceiver : EventsRepository,
    private val intervalsReceiver : IntervalsRepository,
    private val eventId: Id,
    private val intervalId: Id
) : Command<Boolean> {

    private var innerResult : Boolean = false

    override fun getResult() : Boolean {
        return innerResult
    }

    override fun execute() {
        val event = eventsReceiver.get(eventId)
        val interval = intervalsReceiver.get(intervalId)

        if (event != null && interval != null && (event.interval == null || event.interval.id != intervalId)) {
            val updatedEvent = Event(
                event.id,
                event.name,
                event.description,
                interval,
                event.coordinates,
                event.outgoingRelationships,
                event.references
            )

            eventsReceiver.replace(updatedEvent)
            innerResult = true
        }
    }
}