package commands

/**
 * TODO consider add invoker hold by api, then whole pattern asynchronous
 */
interface Command<T> {
    fun execute()

    fun getResult() : T?
}