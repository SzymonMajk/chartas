package commands.update

import commands.Command
import commands.StatusResult
import commands.Status
import domain.temporal.ExactInterval
import domain.temporal.FuzzyInterval
import domain.temporal.Interval
import org.json.JSONObject
import repository.identification.Id
import repository.IntervalsRepository
import java.time.LocalDate

class UpdateIntervalCommand(
    private val receiver: IntervalsRepository,
    private val id: Id,
    private val raw: JSONObject
) : Command<StatusResult<Interval?>> {

    private var innerResult : StatusResult<Interval?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Interval?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)
        val old = receiver.get(id)

        if (old != null && raw.has("date") && raw.has("duration")) {
            val date = LocalDate.parse(raw.getString("date"))

            if (checkDate(date) && checkInt("duration", raw)) {
                if (raw.has("commenceUncertainty") && raw.has("terminationUncertainty")) {
                    if (checkInt("commenceUncertainty", raw) && checkInt("terminationUncertainty", raw)) {
                        val updated = FuzzyInterval(
                                id,
                                date,
                                raw.getInt("duration"),
                                raw.getInt("commenceUncertainty"),
                                raw.getInt("terminationUncertainty")
                            )

                        val fromReceiver = receiver.replace(updated)

                        innerResult = if (fromReceiver != null) {
                            StatusResult(Status.SUCCESS, fromReceiver)
                        } else {
                            StatusResult(Status.FAILURE, null)
                        }
                    } else {
                        innerResult = StatusResult(Status.WRONG_INPUT, null)
                    }
                } else {
                    val updated = ExactInterval(
                        id,
                        date,
                        raw.getInt("duration")
                    )

                    val fromReceiver = receiver.replace(updated)

                    innerResult = if (fromReceiver != null) {
                        StatusResult(Status.SUCCESS, fromReceiver)
                    } else {
                        StatusResult(Status.FAILURE, null)
                    }
                }
            } else {
                innerResult = StatusResult(Status.WRONG_INPUT, null)
            }
        }
    }

    private fun checkDate(date : LocalDate?) : Boolean {
        return date != null
    }

    private fun checkInt(name : String, raw : JSONObject) : Boolean {
        return try {
            raw.getInt(name)
            true
        } catch (e : ClassCastException) {
            false
        }
    }
}