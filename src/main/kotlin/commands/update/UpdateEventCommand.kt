package commands.update

import commands.Command
import commands.Status
import commands.StatusResult
import domain.Event
import export.DomainJsonExporter
import org.json.JSONObject
import repository.EventsRepository
import repository.identification.Id

class UpdateEventCommand(
    private val receiver: EventsRepository,
    private val id: Id,
    private val raw: JSONObject
) : Command<StatusResult<String?>> {

    private var innerResult : StatusResult<String?> = StatusResult(Status.UNLAUNCHED, null)
    private val initialState = "{}"

    override fun getResult() : StatusResult<String?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)
        val old = receiver.get(id)

        if (old != null) {
            if (raw.has("name") && raw.has("description")) {
                val updated = Event(
                    id,
                    raw.getString("name"),
                    raw.getString("description"),
                    old.interval,
                    old.coordinates,
                    old.outgoingRelationships,
                    old.references
                )

                val fromReceiver = receiver.replace(updated)

                innerResult = if (fromReceiver != null) {
                    StatusResult(Status.SUCCESS, fromReceiver.accept(DomainJsonExporter(), initialState))
                } else {
                    StatusResult(Status.FAILURE, null)
                }
            }
        } else {
            innerResult = StatusResult(Status.WRONG_INPUT, null)
        }
    }
}