package commands.update

import commands.Command
import commands.Status
import commands.StatusResult
import domain.Reference
import org.json.JSONObject
import repository.identification.Id
import repository.ReferencesRepository

class UpdateReferenceCommand(
    private val receiver: ReferencesRepository,
    private val id: Id,
    private val raw: JSONObject
) : Command<StatusResult<Reference?>> {

    private var innerResult : StatusResult<Reference?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Reference?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)
        val old = receiver.get(id)

        if (old != null && raw.has("title") && raw.has("author") && raw.has("source")) {
            val updated = Reference(
                id,
                raw.getString("title"),
                raw.getString("author"),
                raw.getString("source")
            )

            val fromReceiver = receiver.replace(updated)

            innerResult = if (fromReceiver != null) {
                StatusResult(Status.SUCCESS, fromReceiver)
            } else {
                StatusResult(Status.FAILURE, null)
            }
        } else {
            innerResult = StatusResult(Status.WRONG_INPUT, null)
        }
    }
}