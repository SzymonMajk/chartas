package commands.update

import com.fasterxml.jackson.databind.ObjectMapper
import commands.Command
import commands.StatusResult
import commands.Status
import domain.spatial.*
import org.json.JSONObject
import repository.CoordinatesRepository
import repository.identification.Id

class UpdateCoordinatesCommand(
    private val receiver: CoordinatesRepository,
    private val id: Id,
    private val raw: JSONObject
) : Command<StatusResult<Coordinates?>> {

    private var innerResult : StatusResult<Coordinates?> = StatusResult(Status.UNLAUNCHED, null)

    override fun getResult() : StatusResult<Coordinates?> {
        return innerResult
    }

    override fun execute() {
        innerResult = StatusResult(Status.ERROR, null)
        val old = receiver.get(id)

        if (old != null && raw.has("coordinates")) {
            val transformer = ArrayTransformer()
            val mapper = ObjectMapper()
            val coords = mapper.readValue(raw.get("coordinates").toString(), Any::class.java)
            val point = transformer.transformPoint(coords)

            if (point != null) {
                val updated = PointCoordinates(
                        id,
                        point
                    )

                val fromReceiver = receiver.replace(updated)

                innerResult = if (fromReceiver != null) {
                    StatusResult(Status.SUCCESS, fromReceiver)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }

            val line = transformer.transformLine(coords)

            if (line != null) {
                val updated = LineCoordinates(
                    id,
                    line
                )

                val fromReceiver = receiver.replace(updated)

                innerResult = if (fromReceiver != null) {
                    StatusResult(Status.SUCCESS, fromReceiver)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }

            val polygons = transformer.transformPolygons(coords)

            if (polygons != null) {
                val updated = PolygonCoordinates(
                    id,
                    polygons
                )

                val fromReceiver = receiver.replace(updated)

                innerResult = if (fromReceiver != null) {
                    StatusResult(Status.SUCCESS, fromReceiver)
                } else {
                    StatusResult(Status.FAILURE, null)
                }
                return
            }
        }

        innerResult = StatusResult(Status.WRONG_INPUT, null)
    }
}