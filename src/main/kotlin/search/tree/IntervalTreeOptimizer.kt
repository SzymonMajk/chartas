package search.tree

import domain.Event
import search.SearchOptimizer
import java.time.LocalDate

/**
 * First creates proper tree structure and then inflate leafs of interval tree
 * with events intersecting with a given leaf.
 */
class IntervalTreeOptimizer(
    childCount : Int = 4,
    depth : Int = 7,
    minimalDate : LocalDate = LocalDate.of(-2000, 1, 1),
    maximalDate : LocalDate = LocalDate.of(4000, 1, 1)
) : SearchOptimizer() {

    private var intervalTree : IntervalNode = IntervalNode(minimalDate, maximalDate)

    init {
        intervalTree.create(depth, childCount)
    }

    /**
     * Finds all nodes and then leafs which intervals intersect with event and then store event
     * in all found leafs.
     */
    override fun add(event : Event) {
        intervalTree.addEvent(event)
    }

    /**
     * Finds all leafs which hold event, then removing it from leaf.
     */
    override fun remove(event : Event) {
        intervalTree.deleteEvent(event)
    }

    /**
     * Start search procedure in already prepared tree, should be called only after successful reset.
     */
    override fun resectVisible(start: LocalDate, end: LocalDate, threshold : Double): Collection<Event> {
        return intervalTree.findIntersecting(start, end)
            .distinctBy { it.id }
    }
}