package search.tree

import domain.Event
import search.SearchUtils
import java.time.LocalDate
import java.time.Period

open class IntervalNode(
    protected open val left : LocalDate,
    protected open val right: LocalDate
) {
    private var subtrees : Collection<IntervalNode> = emptyList()

    private fun calculateSlicePeriod(childCount: Int, wholePeriod : Period) =
        (wholePeriod.years * 365 + wholePeriod.months * 30 + wholePeriod.days) / childCount

    private fun sliceInterval(childCount: Int) : List<Pair<LocalDate, LocalDate>> {
        var results = emptyList<Pair<LocalDate, LocalDate>>()
        val wholePeriod = Period.between(left, right)
        val periodSlice = calculateSlicePeriod(childCount, wholePeriod).toLong()

        for (i in 0 until (childCount - 1)) {
            results = results
                .plus(Pair<LocalDate, LocalDate>(
                    left.plusDays(periodSlice * i),
                    left.plusDays(periodSlice * (i + 1))
                ))
        }

        results = results.plus(Pair<LocalDate, LocalDate>(left.plusDays(periodSlice * (childCount - 1)), right))

        return results
    }

    protected fun eventIntersectWithDates(left : LocalDate, right : LocalDate, event : Event) =
        event.interval != null && SearchUtils.intersect(
            left,
            right,
            event.interval.getLeftBorder(),
            event.interval.getRightBorder()
        )

    fun create(depth : Int, childCount : Int) {
        val sliced = sliceInterval(childCount)

        if (depth == 1) {
            for (i in 0 until childCount) {
                subtrees = subtrees.plus(IntervalLeaf(sliced[i].first, sliced[i].second))
            }
        } else {
            for (i in 0 until childCount) {
                subtrees = subtrees.plus(IntervalNode(sliced[i].first, sliced[i].second))
            }

            subtrees.forEach { it.create(depth - 1, childCount) }
        }
    }

    open fun findIntersecting(left : LocalDate, right : LocalDate) : Collection<Event> {
        var results = emptyList<Event>()

        subtrees
            .filter { SearchUtils.intersect(left, right, it.left, it.right) }
            .forEach { results = results.plus(it.findIntersecting(left, right)) }

        return results
    }

    open fun addEvent(event : Event) {
        subtrees.forEach {
            if (eventIntersectWithDates(left, right, event)) {
                it.addEvent(event)
            }
        }
    }

    open fun deleteEvent(event : Event) {
        subtrees.forEach {
            if (eventIntersectWithDates(left, right, event)) {
                it.deleteEvent(event)
            }
        }
    }
}