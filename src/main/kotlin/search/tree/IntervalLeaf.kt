package search.tree

import domain.Event
import java.time.LocalDate

class IntervalLeaf(
    override val left : LocalDate,
    override val right: LocalDate
) : IntervalNode(left, right) {

    private var intersectingEvents = emptyList<Event>()

    override fun findIntersecting(left : LocalDate, right : LocalDate) : Collection<Event> {
        return intersectingEvents
            .filter { eventIntersectWithDates(left, right, it) }
    }

    override fun addEvent(event : Event) {
        if (eventIntersectWithDates(left, right, event)) {
            intersectingEvents = intersectingEvents.plus(event)
        }
    }

    override fun deleteEvent(event : Event) {
        intersectingEvents = intersectingEvents.minus(event)
    }
}