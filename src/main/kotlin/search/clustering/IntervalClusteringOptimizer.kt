package search.clustering

import domain.Event
import search.SearchOptimizer
import java.time.LocalDate

/**
 * First initializes new list to hold clusters and then iteratively add events.
 */
class IntervalClusteringOptimizer(private val minimalDistance : Int = 1) : SearchOptimizer() {

    private var clusters : List<Cluster> = emptyList()

    private fun mergeClusters(toMerge : List<Cluster>) {
        for (i in 1 until toMerge.size) {
            toMerge[0].events = toMerge[0].events.plus(toMerge[i].events)
            clusters = clusters.minus(toMerge[i])
        }
    }

    /**
     * Every added event could create new cluster or be added to existing one with possible
     * clusters merge. Clusters collect all events intersecting with each other or close in minimal distance.
     */
    override fun add(event: Event) {
        val intersectingClusters = clusters
            .filter { it.eventIntersect(event, minimalDistance.toLong()) }

        when {
            intersectingClusters.size > 1 -> {
                intersectingClusters[0].addEvent(event)
                mergeClusters(intersectingClusters)
            }
            intersectingClusters.size == 1 -> {
                intersectingClusters[0].addEvent(event)
            }
            else -> {
                val newCluster = Cluster(event)
                clusters = clusters.plus(newCluster)
            }
        }
    }

    /**
     * Remove event from all clusters that could hold event with proper changes of border.
     */
    override fun remove(event: Event) {
        clusters
            .filter { it.events.contains(event) }
            .forEach { it.removeEvent(event) }
    }

    /**
     * First takes all clusters that intersect with browser and then retrieve all distinct events from them.
     */
    override fun resectVisible(start: LocalDate, end: LocalDate, threshold : Double): Collection<Event> {
        return clusters
            .filter { it.browserIntersect(start, end) }
            .map { it.events }
            .flatten()
            .distinctBy { it.id }
    }
}