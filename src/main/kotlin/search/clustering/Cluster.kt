package search.clustering

import domain.Event
import search.SearchUtils
import java.time.LocalDate

class Cluster(first : Event) {
    private var left : LocalDate
    private var right : LocalDate
    var events : Collection<Event> = emptyList()

    init {
        events = events.plus(first)
        left = first.interval!!.getLeftBorder(0.0)
        right = first.interval.getRightBorder(0.0)
    }

    private fun updateBorders() {
        left = events.minBy { it.interval!!.getLeftBorder() }!!.interval!!.getLeftBorder()
        right = events.maxBy { it.interval!!.getRightBorder() }!!.interval!!.getRightBorder()
    }

    private fun updateBorders(event : Event) {
        if (event.interval!!.getLeftBorder(0.0).isBefore(left)) {
            left = event.interval.getLeftBorder(0.0)
        }

        if (event.interval.getRightBorder(0.0).isAfter(right)) {
            right = event.interval.getRightBorder(0.0)
        }
    }

    fun eventIntersect(event : Event, minimalDistance : Long) =
        SearchUtils.intersect(
            event.interval!!.getLeftBorder(0.0),
            event.interval.getRightBorder(0.0),
            left.minusDays(minimalDistance),
            right.plusDays(minimalDistance)
        )

    fun browserIntersect(left : LocalDate, right : LocalDate) =
        SearchUtils.intersect(
            left,
            right,
            this.left,
            this.right
        )

    fun addEvent(toAdd : Event) {
        updateBorders(toAdd)
        events = events.plus(toAdd)
    }

    fun removeEvent(toRemove : Event) {
        events = events.minus(toRemove)
        updateBorders()
    }
}