package search

import domain.temporal.Interval
import domain.Event
import java.time.LocalDate

/**
 * Template pattern for adapting various optimization algorithms for search.
 */
abstract class SearchOptimizer {

    /**
     * Backward and forward visibilities are used because we assume that
     * events intersecting even on borders should be visible.
     */
    private fun isVisible(interval: Interval, begin: LocalDate, end: LocalDate, threshold : Double) =
        SearchUtils.intersect(
            begin,
            end,
            interval.getLeftBorder(threshold),
            interval.getRightBorder(threshold))

    /**
     * Skeleton of search algorithm, should not be override.
     */
    fun getVisible(start : LocalDate, end : LocalDate, threshold: Double = 0.0) : Collection<Event> {
        return if (start.isAfter(end)) {
            resectVisible(end, start, threshold)
                .filter { it.interval != null && isVisible(it.interval, end, start, threshold) }
        } else {
            resectVisible(start, end, threshold)
                .filter { it.interval != null && isVisible(it.interval, start, end, threshold) }
        }
    }

    /**
     * Implementation should be able to iteratively add new events to its inner storage.
     */
    abstract fun add(event : Event)

    /**
     * Implementation should be able to iteratively remove event that is no longer in usage.
     */
    abstract fun remove(event : Event)

    /**
     * Implementations are able to use various techniques to decrease search space.
     */
    abstract fun resectVisible(start : LocalDate, end : LocalDate, threshold : Double) : Collection<Event>
}