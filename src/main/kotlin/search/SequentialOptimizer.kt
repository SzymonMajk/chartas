package search

import domain.Event
import java.time.LocalDate

/**
 * Basic implementation with no optimization.
 */
class SequentialOptimizer : SearchOptimizer() {

    private var events : Collection<Event> = emptyList()

    /**
     * Simply add event to private list.
     */
    override fun add(event: Event) {
        this.events = events.plus(event)
    }

    /**
     * Simply remove event from private list.
     */
    override fun remove(event: Event) {
        this.events = events.minus(event)
    }

    /**
     * Just return owned collection.
     */
    override fun resectVisible(start: LocalDate, end: LocalDate, threshold : Double): Collection<Event> {
        return events
    }
}