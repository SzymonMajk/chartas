package search

import java.time.LocalDate

class SearchUtils {
    companion object {
        fun intersect(
            firstLeft : LocalDate,
            firstRight : LocalDate,
            secondLeft : LocalDate,
            secondRight : LocalDate
        ) = firstRight.isAfter(secondLeft) && firstLeft.isBefore(secondRight)
    }
}