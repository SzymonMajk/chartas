package search.bisection

import domain.Event
import search.SearchUtils
import java.time.LocalDate

class Segment(val event : Event) {
    lateinit var left : LocalDate
    lateinit var right : LocalDate

    init {
        if (event.interval != null) {
            left = event.interval.getLeftBorder()
            right = event.interval.getRightBorder()
        }
    }

    var intersecting = emptySet<Event>()

    fun addIntersecting(events : Collection<Event>) {
        intersecting = intersecting.plus(events)
    }

    fun addIntersecting(event : Event) {
        intersecting = intersecting.plus(event)
    }

    fun eraseEvent(event : Event) {
        intersecting = intersecting.minus(event)
    }
}