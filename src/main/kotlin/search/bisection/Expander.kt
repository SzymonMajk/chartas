package search.bisection

import domain.Event
import java.time.LocalDate

class Expander(private val sortedSegments : List<Segment>) {

    fun expandLeft(startingIndex : Int, leftBorder : LocalDate) : List<Event> {
        if (startingIndex == 0) {
            return emptyList()
        }

        var currentIndex = startingIndex - 1
        var results = emptyList<Event>()

        while (currentIndex >= 0) {
            val currentRightBound = sortedSegments[currentIndex].right

            if (leftBorder.isBefore(currentRightBound)) {
                results = results
                    .plus(sortedSegments[currentIndex].event)
                    .plus(sortedSegments[currentIndex].intersecting)
            } else {
                return results
            }

            currentIndex -= 1
        }

        return results
    }

    fun expandEventLeft(startingIndex: Int, event : Event, leftBorder: LocalDate) {
        if (startingIndex <= 1) {
            return
        }

        var currentIndex = startingIndex - 2

        while (currentIndex >= 0) {
            val currentRightBound = sortedSegments[currentIndex].right

            if (leftBorder.isBefore(currentRightBound)) {
                sortedSegments[currentIndex].addIntersecting(event)
            } else {
                break
            }

            currentIndex -= 1
        }
    }

    fun expandEraseEventLeft(startingIndex: Int, event : Event, leftBorder: LocalDate) {
        if (startingIndex <= 1) {
            return
        }

        var currentIndex = startingIndex - 2

        while (currentIndex >= 0) {
            val currentRightBound = sortedSegments[currentIndex].right

            if (leftBorder.isBefore(currentRightBound)) {
                sortedSegments[currentIndex].eraseEvent(event)
            } else {
                break
            }

            currentIndex -= 1
        }
    }

    fun expandRight(startingIndex : Int, rightBorder : LocalDate) : List<Event> {
        if (startingIndex == 0) {
            return emptyList()
        }

        var currentIndex = startingIndex - 1
        var results = emptyList<Event>()

        while (currentIndex <= (sortedSegments.size - 1)) {
            val currentLeftBound = sortedSegments[currentIndex].left

            if (rightBorder.isAfter(currentLeftBound)) {
                results = results
                    .plus(sortedSegments[currentIndex].event)
                    .plus(sortedSegments[currentIndex].intersecting)
            } else {
                return results
            }

            currentIndex += 1
        }

        return results
    }

    fun expandEventRight(startingIndex : Int, event : Event, rightBorder : LocalDate) {
        if (startingIndex == 0 || startingIndex == sortedSegments.size) {
            return
        }

        var currentIndex = startingIndex

        while (currentIndex <= (sortedSegments.size - 1)) {
            val currentLeftBound = sortedSegments[currentIndex].left

            if (rightBorder.isAfter(currentLeftBound)) {
                sortedSegments[currentIndex].addIntersecting(event)
            } else {
                break
            }

            currentIndex += 1
        }
    }

    fun expandEraseEventRight(startingIndex : Int, event : Event, rightBorder : LocalDate) {
        if (startingIndex == 0 || startingIndex == sortedSegments.size) {
            return
        }

        var currentIndex = startingIndex

        while (currentIndex <= (sortedSegments.size - 1)) {
            val currentLeftBound = sortedSegments[currentIndex].left

            if (rightBorder.isAfter(currentLeftBound)) {
                sortedSegments[currentIndex].eraseEvent(event)
            } else {
                break
            }

            currentIndex += 1
        }
    }
}