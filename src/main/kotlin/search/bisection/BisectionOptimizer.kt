package search.bisection

import domain.Event
import search.SearchOptimizer
import java.time.LocalDate

/**
 * First transform events into section before stored in ordered list then each event from list
 * is added with proper transformations as mentioned in add function.
 */
class BisectionOptimizer : SearchOptimizer() {

    private var segments : MutableList<Segment> = arrayListOf()
    private var searcher = Searcher(segments)
    private var expander = Expander(segments)

    private fun collectIntersectingWith(event : Event, index : Int) : Collection<Event> =
        expander.expandLeft(index, event.interval!!.getLeftBorder())
            .plus(expander.expandRight(index, event.interval.getRightBorder()))

    /**
     * First uses searcher to find proper index for new event. Then uses expander, first to retrieve
     * all events intersecting with new one around its new place, then to add new event into segments around.
     */
    override fun add(event : Event) {
        val segment = Segment(event)
        val index = searcher.findIndexForDate(event.interval!!.getLeftBorder())

        segment.addIntersecting(collectIntersectingWith(event, index))
        segments.add(index, segment)
        expander.expandEventLeft(index, event, event.interval.getLeftBorder())
        expander.expandEventRight(index, event, event.interval.getRightBorder())
    }

    /**
     * First uses searcher to find out if event exist in storage and then use its index to erase
     * this event from all segments around and then remove segment holding this event.
     */
    override fun remove(event : Event) {
        val index = searcher.findIndexOfDate(event.interval!!.getLeftBorder())

        if (index != null) {
            expander.expandEraseEventLeft(index, event, event.interval.getLeftBorder())
            expander.expandEraseEventRight(index, event, event.interval.getRightBorder())
            segments.removeAt(index)
        }
    }

    /**
     * First find event between start and end, then look ahead and back until first invisible found,
     * should be called only after successful reset function.
     */
    override fun resectVisible(start: LocalDate, end: LocalDate, threshold : Double): Collection<Event> {
        val index = searcher.findIntersectingIndex(start, end)

        return if (index != null) {
            listOf(segments[index].event)
                .plus(segments[index].intersecting)
                .plus(expander.expandLeft(index, start))
                .plus(expander.expandRight(index, end))
                .distinctBy { it.id }
        } else {
            emptyList()
        }
    }
}