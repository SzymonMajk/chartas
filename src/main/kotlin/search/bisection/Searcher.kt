package search.bisection


import search.SearchUtils
import java.time.LocalDate

class Searcher(private val sortedSegments : List<Segment>) {

    fun findIntersectingIndex(leftBrowser : LocalDate, rightBrowser: LocalDate) : Int? {
        var leftBoundIndex = 0
        var rightBoundIndex = sortedSegments.size
        var currentMiddleIndex = (leftBoundIndex + rightBoundIndex) / 2

        do {
            val currentMiddleEventLeft = sortedSegments[currentMiddleIndex].left
            val currentMiddleEventRight = sortedSegments[currentMiddleIndex].right

            if (SearchUtils.intersect(leftBrowser, rightBrowser, currentMiddleEventLeft, currentMiddleEventRight)) {
                return currentMiddleIndex
            } else {
                if (leftBrowser.isBefore(currentMiddleEventLeft)) {
                    rightBoundIndex = currentMiddleIndex
                } else {
                    leftBoundIndex = currentMiddleIndex
                }

                currentMiddleIndex = (leftBoundIndex + rightBoundIndex) / 2
            }
        } while (currentMiddleIndex != leftBoundIndex && currentMiddleIndex != rightBoundIndex)

        return null
    }

    fun findIndexOfDate(date : LocalDate) : Int? {
        var leftBoundIndex = 0
        var rightBoundIndex = sortedSegments.size
        var currentMiddleIndex = (leftBoundIndex + rightBoundIndex + 1) / 2

        do {
            val currentMiddleEventLeft = sortedSegments[currentMiddleIndex].left

            if (currentMiddleEventLeft == date) {
                return currentMiddleIndex
            } else {
                if (date.isBefore(currentMiddleEventLeft)) {
                    rightBoundIndex = currentMiddleIndex
                } else {
                    leftBoundIndex = currentMiddleIndex
                }

                currentMiddleIndex = (leftBoundIndex + rightBoundIndex + 1) / 2
            }
        } while (currentMiddleIndex != leftBoundIndex && currentMiddleIndex != rightBoundIndex)

        return null
    }

    fun findIndexForDate(date : LocalDate) : Int {
        var leftBoundIndex = 0
        var rightBoundIndex = sortedSegments.size - 1
        var currentMiddleIndex = (leftBoundIndex + rightBoundIndex + 1) / 2

        if (sortedSegments.isEmpty() || date.isBefore(sortedSegments[0].left)) {
            return 0
        }

        if (date.isAfter(sortedSegments[sortedSegments.size - 1].left)) {
            return sortedSegments.size
        }

        if (sortedSegments.size == 2) {
            return 1
        }

        do {
            val currentMiddleEventLeft = sortedSegments[currentMiddleIndex].left
            val currentBeforeMiddleEventLeft = sortedSegments[currentMiddleIndex - 1].left
            val currentAfterMiddleEventLeft = sortedSegments[currentMiddleIndex + 1].left

            if (currentBeforeMiddleEventLeft.isBefore(date) && currentAfterMiddleEventLeft.isAfter(date)) {
                return if (currentMiddleEventLeft.isBefore(date)) {
                    currentMiddleIndex + 1
                } else {
                    currentMiddleIndex
                }
            } else {
                if (date.isBefore(currentMiddleEventLeft)) {
                    rightBoundIndex = currentMiddleIndex
                } else {
                    leftBoundIndex = currentMiddleIndex
                }

                currentMiddleIndex = (leftBoundIndex + rightBoundIndex + 1) / 2
            }
        } while (currentMiddleIndex != leftBoundIndex && currentMiddleIndex != rightBoundIndex)

        return currentMiddleIndex
    }

}